const AWS_REGION = process.env.AWS_REGION;
const TABLE_NUMBER1 = process.env.TABLE_NUMBER1;
const TABLE_USER_EXTEND = process.env.TABLE_USER_EXTEND; 

const AWS = require("aws-sdk");
const docClient = new AWS.DynamoDB.DocumentClient({region: AWS_REGION});

exports.handle = (event, context,callback) => {
    var allw8 = [];
    console.log('Received event:', JSON.stringify(event, null, 2));
    event.Records.forEach((record) => {
        console.log(record.eventID);
        console.log(record.eventName);
        console.log('DynamoDB Record: %j', record.dynamodb);
        if(record.eventName==="INSERT"){  // add number
            if(record.dynamodb.NewImage.sk.S.startsWith("a#")===true){
                allw8.push(updateSumadd(record.dynamodb,'add'));
            }else{
                allw8.push(Promise.resolve());
            }

            if( (record.dynamodb.NewImage.sk.S.startsWith("a#")===true)
               && ('confirm' in record.dynamodb.NewImage) 
               && ('N' in record.dynamodb.NewImage.confirm)){
                allw8.push(updateN1Sum(record.dynamodb));
                allw8.push(updateSumOnHand(record.dynamodb));
            }
        }else if(record.eventName==="MODIFY"){
            if( !('confirm' in record.dynamodb.OldImage) 
            && ('confirm' in record.dynamodb.NewImage) 
            && ('N' in record.dynamodb.NewImage.confirm) ){  //confirm number
                allw8.push(updateN1Sum(record.dynamodb));
                allw8.push(updateSumOnHand(record.dynamodb));
            }else if( !('jackpots' in record.dynamodb.OldImage) 
            && ('jackpots' in record.dynamodb.NewImage) ) { //add new jackpot

                allw8.push(updateJackpots(record.dynamodb,'add'));
                allw8.push(updatePayJackpots(record.dynamodb,'add'));

            }else if( ('jackpots' in record.dynamodb.OldImage) 
            && !('jackpots' in record.dynamodb.NewImage) ) { //remove jackpot

                allw8.push(updateJackpots(record.dynamodb,'remove'));
                allw8.push(updatePayJackpots(record.dynamodb,'remove'));

            }else if( ('jackpots' in record.dynamodb.OldImage) 
            && ('jackpots' in record.dynamodb.NewImage) ) { //replace jackpot

                allw8.push(updateJackpots(record.dynamodb,'remove'));
                allw8.push(updatePayJackpots(record.dynamodb,'remove'));
                allw8.push(updateJackpots(record.dynamodb,'add'));
                allw8.push(updatePayJackpots(record.dynamodb,'add'));

            }else{
                allw8.push(Promise.resolve());
            }
        }else if(record.eventName==="REMOVE"){
            if(record.dynamodb.OldImage.sk.S.startsWith("a#")===true){
                allw8.push(updateSumadd(record.dynamodb,'remove'));
            }else{
                allw8.push(Promise.resolve());
            }
        }
    });
    Promise.all(allw8).then(function(){
        console.log(`Successfully processed ${event.Records.length} records.`);
        callback(null,'ok');
    });
};

function updateSumadd(item,ops='add'){
    return new Promise((resolve, reject) => {
        let params
        if(ops==='add')
            params = {
                TableName : TABLE_NUMBER1+"_"+item.NewImage.periods.S,
                Key : {
                    owner: item.NewImage.owner.S,
                    sk : "sumadd"
                },
                UpdateExpression: "ADD #amountset :amountset , #price :price",
                ExpressionAttributeNames:{
                    "#amountset":"amountset",
                    "#price":"price"
                },
                ExpressionAttributeValues:{
                    ":amountset":Number(item.NewImage.amountset.N),
                    ":price":Number(item.NewImage.price.N)
                },
                ReturnValues:"NONE"
            };
        else{
            params = {
                TableName : TABLE_NUMBER1+"_"+item.OldImage.periods.S,
                Key : {
                    owner: item.OldImage.owner.S,
                    sk : "sumadd"
                },
                UpdateExpression: "ADD #amountset :amountset , #price :price",
                ExpressionAttributeNames:{
                    "#amountset":"amountset",
                    "#price":"price"
                },
                ExpressionAttributeValues:{
                    ":amountset":Number(item.OldImage.amountset.N)*-1,
                    ":price":Number(item.OldImage.price.N)*-1
                },
                ReturnValues:"NONE"
            };
        }
        docClient.update(params, function(err, data) {
            if (err) {
                let errMsg = "Unable to update Sumadd item. Error JSON:"+ JSON.stringify(err, null, 2);
                console.log(errMsg);
                resolve();
            } else {
                resolve();    
            }
        });
    });
}
function updateN1Sum(item){
    return new Promise((resolve, reject) => {
        let params = {
            TableName : TABLE_NUMBER1+"_"+item.NewImage.periods.S,
            Key : {
                owner: item.NewImage.topowner.S,
                sk : "s#"+item.NewImage.number.S,
            },
            UpdateExpression: "SET #num = :num ADD #amountset :amountset , #price :price",
            ExpressionAttributeNames:{
                "#num":"number",
                "#amountset":"amountset",
                "#price":"price"
            },
            ExpressionAttributeValues:{
                ":num":item.NewImage.number.S,
                ":amountset":Number(item.NewImage.amountset.N),
                ":price":Number(item.NewImage.price.N)
            },
            ReturnValues:"NONE"
        };

        docClient.update(params, function(err, data) {
            if (err) {
                let errMsg = "Unable to update Sumadd item. Error JSON:"+ JSON.stringify(err, null, 2);
                console.log(errMsg);
                resolve();
            } else {
                resolve();    
            }
        });
    });
}
function updateSumOnHand(item){
    return new Promise((resolve, reject) => {
        let params = {
            TableName : TABLE_NUMBER1+"_"+item.NewImage.periods.S,
            Key : {
                owner: item.NewImage.topowner.S,
                sk : "sumonhand",
            },
            UpdateExpression: "ADD #amountset :amountset , #price :price",
            ExpressionAttributeNames:{
                "#amountset":"amountset",
                "#price":"price"
            },
            ExpressionAttributeValues:{
                ":amountset":Number(item.NewImage.amountset.N),
                ":price":Number(item.NewImage.price.N)
            },
            ReturnValues:"NONE"
        };

        docClient.update(params, function(err, data) {
            if (err) {
                let errMsg = "Unable to update Sumadd item. Error JSON:"+ JSON.stringify(err, null, 2);
                console.log(errMsg);
                resolve();
            } else {
                resolve();    
            }
        });
    });
}
function updateJackpots(item,ops='add'){
    return new Promise((resolve, reject) => {
        let params,amountset,prize,arr;
        if(ops==='add'){
            amountset = Number(item.NewImage.amountset.N);
            prize = Number(item.NewImage.prizepool.M[item.NewImage.jackpots.S].N)*Number(item.NewImage.amountset.N);
            arr = item.NewImage.jackpots.S+'X'+item.NewImage.prizepool.M[item.NewImage.jackpots.S].N
        }else{
            amountset = Number(item.OldImage.amountset.N)*-1;
            prize = Number(item.OldImage.prizepool.M[item.OldImage.jackpots.S].N)*Number(item.OldImage.amountset.N)*-1;
            arr = item.OldImage.jackpots.S+'X'+item.OldImage.prizepool.M[item.OldImage.jackpots.S].N
        }
        params = {
            TableName : TABLE_NUMBER1+"_"+item.NewImage.periods.S,
            Key : {
                owner: item.NewImage.owner.S,
                sk : "jackpots"
            },
            UpdateExpression: "SET #topowner = :topowner ADD #amountset :amountset , #prize :prize, #arr :amountset",
            ExpressionAttributeNames:{
                "#topowner":"topowner",
                "#amountset":"amountset",
                "#prize":"prize",
                "#arr": arr
            },
            ExpressionAttributeValues:{
                ":topowner":item.NewImage.topowner.S,
                ":amountset":amountset,
                ":prize":prize
            },
            ReturnValues:"NONE"
        };
        docClient.update(params, function(err, data) {
            if (err) {
                let errMsg = "Unable to update jackpots item. Error JSON:"+ JSON.stringify(err, null, 2);
                console.log(errMsg);
                resolve();
            } else {
                resolve();    
            }
        });
    });
}
function updatePayJackpots(item,ops='add'){
    return new Promise((resolve, reject) => {
        let params,amountset,prize,arr;
        if(ops==='add'){
            amountset = Number(item.NewImage.amountset.N);
            prize = Number(item.NewImage.prizepool.M[item.NewImage.jackpots.S].N)*Number(item.NewImage.amountset.N);
            arr = item.NewImage.jackpots.S+'X'+item.NewImage.prizepool.M[item.NewImage.jackpots.S].N
        }else{
            amountset = Number(item.OldImage.amountset.N)*-1;
            prize = Number(item.OldImage.prizepool.M[item.OldImage.jackpots.S].N)*Number(item.OldImage.amountset.N)*-1;
            arr = item.OldImage.jackpots.S+'X'+item.OldImage.prizepool.M[item.OldImage.jackpots.S].N
        }
        params = {
            TableName : TABLE_NUMBER1+"_"+item.NewImage.periods.S,
            Key : {
                owner: item.NewImage.topowner.S,
                sk : "payjackpots"
            },
            UpdateExpression: "ADD #amountset :amountset , #prize :prize, #arr :amountset",
            ExpressionAttributeNames:{
                "#amountset":"amountset",
                "#prize":"prize",
                "#arr": arr
            },
            ExpressionAttributeValues:{
                ":amountset":amountset,
                ":prize":prize
            },
            ReturnValues:"NONE"
        };
        docClient.update(params, function(err, data) {
            if (err) {
                let errMsg = "Unable to update jackpots item. Error JSON:"+ JSON.stringify(err, null, 2);
                console.log(errMsg);
                resolve();
            } else {
                resolve();    
            }
        });
    });
}
// function updateN1Sum(item){
//     return new Promise((resolve, reject) => {
//         let params = {
//             TableName : TABLE_NUMBER1SUM,
//             Key : {
//                 owner: item.NewImage.toplvl.S,
//                 periodsnumber : item.NewImage.periods.S+"#"+item.NewImage.number.N
//             },
//             UpdateExpression: "ADD #amountset :amountset , #price :price SET #num = :num",
//             ExpressionAttributeNames:{
//                 "#num":"number",
//                 "#amountset":"amountset",
//                 "#price":"price"
//             },
//             ExpressionAttributeValues:{
//                 ":num":Number(item.NewImage.number.N),
//                 ":amountset":Number(item.NewImage.amountset.N),
//                 ":price":Number(item.NewImage.price.N)
//             },
//             ReturnValues:"NONE"
//         };
//         docClient.update(params, function(err, data) {
//             if (err) {
//                 if(err.code === "ValidationException" && err.message === "The provided expression refers to an attribute that does not exist in the item"){
//                     //addN1Sum(item).then(function(){
//                         resolve();
//                     //});
//                 }else{
//                     let errMsg = "Unable to update usedcredit item. Error JSON:"+ JSON.stringify(err, null, 2);
//                     console.log(errMsg);
//                     resolve();
//                 }
//             } else {
//                 resolve();    
//             }
//         });
//     });
// }
// function updateUsedCredit(lowItem){
//     return new Promise((resolve, reject) => {
//         let params = {
//             TableName : TABLE_USER_EXTEND,
//             Key : {
//                 username : lowItem.owner.S,
//                 extend:"number1#credit"
//             },
//             UpdateExpression: "set #usedcredit = #usedcredit + :price",
//             ExpressionAttributeNames:{"#usedcredit":"usedcredit"},
//             ExpressionAttributeValues:{":price":Number(lowItem.price.N)},
//             ReturnValues:"NONE"
//         };
//         docClient.update(params, function(err, data) {
//             if (err) {
//                 let errMsg = "Unable to update usedcredit item. Error JSON:"+ JSON.stringify(err, null, 2);
//                 console.log(errMsg);
//                 resolve();
//             } else {
//                 resolve();    
//             }
//         });
//     });

// }