const AWS_REGION = process.env.AWS_REGION;
const ACCESS_CONTROL_ALLOW_ORIGIN = process.env.ACCESS_CONTROL_ALLOW_ORIGIN;
const TABLE_NUMBER1 = process.env.TABLE_NUMBER1;
const INDEX_NUMBER1_JACKPOTS = process.env.INDEX_NUMBER1_JACKPOTS;
const TABLE_USER = process.env.TABLE_USER;


const AWS = require("aws-sdk");
const docClient = new AWS.DynamoDB.DocumentClient({region: AWS_REGION});

var ResponseBadRequest = {
    statusCode: 400,
    body:JSON.stringify({"message":"Bad Request"}),
    headers:{
        "Access-Control-Allow-Origin":ACCESS_CONTROL_ALLOW_ORIGIN
    }
};

exports.handle = async function(event, context) {
    let queryStringParameters = event.queryStringParameters;
    let userAuthorized = event.requestContext.authorizer;
    try {
        if(event.httpMethod==="GET"){ //&& event.pathParameters===null){
            if(queryStringParameters.startkey===undefined){
                return await getAll(userAuthorized.principalId,queryStringParameters);
            }else{
                let startkeyjson = new Buffer(queryStringParameters.startkey, 'base64').toString('ascii');
                let ExclusiveStartKey = JSON.parse(startkeyjson);
                return await getAll(userAuthorized.principalId,queryStringParameters,ExclusiveStartKey);
            }
        }
        // else if(event.httpMethod==="GET" && !(event.pathParameters===null)){
        //     let chk = await checkDownline(event.pathParameters.username,userAuthorized.principalId)
        //     if(chk===false){
        //         return ResponseBadRequest;
        //     }
        //     if(queryStringParameters.startkey===undefined){
        //         return await getAll(event.pathParameters.username,queryStringParameters);
        //     }else{
        //         let startkeyjson = new Buffer(queryStringParameters.startkey, 'base64').toString('ascii');
        //         let ExclusiveStartKey = JSON.parse(startkeyjson);
        //         return await getAll(event.pathParameters.username,queryStringParameters,ExclusiveStartKey);
        //     }
        // }
        else{
            console.error("httpMethod not match");
        }
    }catch (error) {
        console.error(error);
        throw new Error(error);
    }
};

function getAll(user,queryStringParameters,ExclusiveStartKey=null){
    return new Promise(async (resolve, reject) => {
        let params = {
            IndexName : INDEX_NUMBER1_JACKPOTS,
            TableName : TABLE_NUMBER1+"_"+queryStringParameters.periods,
            KeyConditionExpression :"#owner = :owner",
            ExpressionAttributeValues : {
                ":owner": user,
            },
            ExpressionAttributeNames : {
                "#owner": "owner",
                "#number": "number"
            },
            ProjectionExpression :"#number,bill,createdate,amountset,priceset,price,jackpots"
        };
        if('limit' in queryStringParameters && Number.isInteger(Number(queryStringParameters.limit)) ){
            params.Limit = Number(queryStringParameters.limit)
        }
        if(!(ExclusiveStartKey===null)){
            params.ExclusiveStartKey = ExclusiveStartKey;
        }
        if(!('scanforward' in queryStringParameters) && queryStringParameters.scanforward==="no"){
            params.ScanIndexForward = false;
        }else{
            params.ScanIndexForward = true;
        }
        console.log(params);
        docClient.query(params, function(err, data) {
            if (err) {
                if(err.code === "ResourceNotFoundException"){
                    resolve(ResponseBadRequest);
                }else{
                    let errMsg = "Unable to getall item. Error JSON:"+ JSON.stringify(err, null, 2);
                    console.log(errMsg);
                    reject(errMsg);
                }
            } else {
                delete data.ScannedCount;
                if(!(data.LastEvaluatedKey===undefined)){
                    let Lk = data.LastEvaluatedKey;
                    let Lkjson = JSON.stringify(Lk);
                    let newLk = new Buffer(Lkjson).toString('base64');
                    data.LastEvaluatedKey = newLk;
                }
                resolve({
                    statusCode: 200,
                    headers:{
                        "Access-Control-Allow-Origin":ACCESS_CONTROL_ALLOW_ORIGIN
                    },
                    body:JSON.stringify(data)
                });    
            }
        });
    });
}

function checkDownline(down,top){
    return new Promise((resolve, reject) => {
        let params = {
            TableName : TABLE_USER,
            Key : {
                username : down
            }
        };
        docClient.get(params, function(err, data) {
            if (err) {
                let errMsg = "Unable to read item. Error JSON:"+ JSON.stringify(err, null, 2);
                reject(errMsg);
            } else {
                if(data.Item === undefined){
                    resolve(false);
                }else{
                    if(data.Item.toplvl===top){
                        resolve(true);
                    }else{
                        resolve(false);
                    }
                }
                
                   
            }
        });
    });
}