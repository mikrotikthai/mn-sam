const AWS_REGION = process.env.AWS_REGION;
const ACCESS_CONTROL_ALLOW_ORIGIN = process.env.ACCESS_CONTROL_ALLOW_ORIGIN;
const TABLE_NUMBER1 = process.env.TABLE_NUMBER1;
const TABLE_USER_EXTEND = process.env.TABLE_USER_EXTEND; 

const AWS = require("aws-sdk");
const docClient = new AWS.DynamoDB.DocumentClient({region: AWS_REGION});
var ResponseCreate = {
    statusCode: 201,
    body:JSON.stringify({"message":"ok"}),
    headers:{
        "Access-Control-Allow-Origin":ACCESS_CONTROL_ALLOW_ORIGIN
    }
};
var ResponseOk = {
    statusCode: 200,
    body:JSON.stringify({"message":"ok"}),
    headers:{
        "Access-Control-Allow-Origin":ACCESS_CONTROL_ALLOW_ORIGIN
    }
};
var ResponseConfirmError = {
    statusCode: 400,
    body:JSON.stringify({"message":"invalid periods"}),
    headers:{
        "Access-Control-Allow-Origin":ACCESS_CONTROL_ALLOW_ORIGIN
    }
};
var ResponseBadRequest = {
    statusCode: 400,
    body:JSON.stringify({"message":"Bad Request"}),
    headers:{
        "Access-Control-Allow-Origin":ACCESS_CONTROL_ALLOW_ORIGIN
    }
};
var ResponseNotAllow = {// not have credit
    statusCode: 405,
    body:JSON.stringify({"message":"Method Not Allowed"}),
    headers:{
        "Access-Control-Allow-Origin":ACCESS_CONTROL_ALLOW_ORIGIN
    }
};
var ResponseNotAccept = {// missing config
    statusCode: 406,
    body:JSON.stringify({"message":"Game close or Missing config"}),
    headers:{
        "Access-Control-Allow-Origin":ACCESS_CONTROL_ALLOW_ORIGIN
    }
};
var ResponseGameEnd = {// game end
    statusCode: 410,
    body:JSON.stringify({"message":"Game is end"}),
    headers:{
        "Access-Control-Allow-Origin":ACCESS_CONTROL_ALLOW_ORIGIN
    }
};


exports.handle = async function(event, context) {
    // console.log(JSON.stringify(event));
    let bodyjson = JSON.parse(event.body);
    let userAuthorized = event.requestContext.authorizer;
    try {
        if(event.httpMethod==="POST"){
            if(checkPeriods(bodyjson.periods)===true){
                let chkresult = await getConfig(userAuthorized,bodyjson);
                if(chkresult.status === false){
                    return ResponseNotAccept;
                }else{
                    let res = 0;
                    console.log(JSON.stringify(chkresult));
                    for(var i=0;i<bodyjson.send.length;i++){
                        console.log(i)
                        res += await send(bodyjson.send[i],bodyjson.periods,chkresult,userAuthorized)
                    }
                    if(res!==0){
                        await updateBill(userAuthorized);
                    }
                    console.log('end')
                    return ResponseOk;
                }
                
            }else{
                return ResponseConfirmError;
            }
        }else{
            console.error("httpMethod not match");
        }
    }catch (error) {
        console.error(error);
        throw new Error(error);
    }
};
function getConfig(userAuthorized,body){
    return new Promise((resolve, reject) => {
        let params = {
            RequestItems: {}
        };
        params.RequestItems[TABLE_USER_EXTEND] ={
            Keys: [
                {
                    username:userAuthorized.toplvl,
                    extend:"number1#gameconfig#global"
                },
                {
                    username:userAuthorized.principalId,
                    extend:"number1#gameconfig#indi"
                },
                {
                    username:userAuthorized.principalId,
                    extend:"number1#bill"
                }
            ]
        }
        docClient.batchGet(params, function(err, data) {
            let result = {};
            if (err){
                if(err.code === "ResourceNotFoundException"){
                    result.status = false;
                    result.code = "406";
                    resolve(result);
                }else{
                    let errMsg = "Unable to getconfig items. Error JSON:"+ JSON.stringify(err, null, 2);
                    console.log(errMsg);
                    reject(errMsg);
                }
                
            }else {
                console.log(JSON.stringify(data));
                let res = data.Responses;
                if( res[TABLE_USER_EXTEND].length === 0){
                    result.status = false;
                    result.code = "406";
                    resolve(result);
                }else{
                    let config = {};
                    for(var i = 0; i< res[TABLE_USER_EXTEND].length ; i++){
                        if(res[TABLE_USER_EXTEND][i].extend==="number1#gameconfig#global"){
                            if(!(res[TABLE_USER_EXTEND][i].priceset===undefined) && !('priceset' in config) ){
                                config.priceset = res[TABLE_USER_EXTEND][i].priceset;
                            }
                            if(!(res[TABLE_USER_EXTEND][i].status===undefined) && !('status' in config)){
                                config.status = res[TABLE_USER_EXTEND][i].status;
                            }
                            if(!(res[TABLE_USER_EXTEND][i].closetime===undefined) && !('closetime' in config)){
                                config.closetime = res[TABLE_USER_EXTEND][i].closetime;
                            }  
                            if(!(res[TABLE_USER_EXTEND][i].prizepool===undefined) && !('prizepool' in config)){
                                config.prizepool = res[TABLE_USER_EXTEND][i].prizepool;
                            }   
                        }else if(res[TABLE_USER_EXTEND][i].extend==="number1#gameconfig#indi"){
                            if(!(res[TABLE_USER_EXTEND][i].priceset===undefined)){
                                config.priceset = res[TABLE_USER_EXTEND][i].priceset;
                            }
                            if(!(res[TABLE_USER_EXTEND][i].status===undefined)){
                                config.status = res[TABLE_USER_EXTEND][i].status;
                            }
                            if(!(res[TABLE_USER_EXTEND][i].closetime===undefined)){
                                config.closetime = res[TABLE_USER_EXTEND][i].closetime;
                            }
                            if(!(res[TABLE_USER_EXTEND][i].prizepool===undefined)){
                                config.prizepool = res[TABLE_USER_EXTEND][i].prizepool;
                            }   
                        }else if(res[TABLE_USER_EXTEND][i].extend==="number1#bill"){
                            config.bill = res[TABLE_USER_EXTEND][i].bill;
                        }
                    }
                    if(config.bill===undefined){
                        result.bill = 0;
                    }else{
                        result.bill = config.bill;
                    }
                    console.log(config)
                    if( !(config.status===undefined) && (config.status==="open") ){
                        if( !(config.closetime===undefined) 
                            && (config.closetime>Date.now()) 
                            && !(config.prizepool===undefined) 
                            ){
                            result.prizepool = config.prizepool
                            if(config.priceset === undefined){
                                result.priceset = 120;
                            }else{
                                result.priceset = config.priceset;
                            }
                            result.status = true;
                            resolve(result);
                        }else{
                            result.status = false;
                            result.code = "406";
                            resolve(result);
                        }
                    }else{
                        result.status = false;
                        result.code = "406";
                        resolve(result);
                    }
                    
                }
            }
        });
    });
}
function send(item,periods,chkresultconfig,userAuthorized){
    return new Promise((resolve, reject) => {
        addSend(item,periods,userAuthorized,chkresultconfig.bill).then(function(bill){
            let body={};
            Object.assign(body,item)
            body.bill = chkresultconfig.bill;
            body.periods = periods;
            return addNumber(body,userAuthorized,chkresultconfig)
        }).then(function(){
            resolve(1)
        }).catch(function(){
            console.log('error to send '+body.number)
            resolve(0)
        })
    });
}
function addSend(body,periods,userAuthorized,bill,newlist=false){
    return new Promise((resolve, reject) => {
        let sendround = [];
        sendround.push({"bill":bill,"amountset":body.amountset});
        let params = {
            TableName : TABLE_NUMBER1+"_"+periods,
            Key : {
                owner : userAuthorized.principalId,
                sk : "s#"+body.number
            },
            ConditionExpression:"attribute_exists(#owner) and attribute_exists(#sk)", 
            UpdateExpression: "ADD #send :send SET #sendround = list_append(#sendround,:sendround)",
            ExpressionAttributeNames:{
                "#send":"send",
                "#sendround":"sendround",
                "#owner":"owner",
                "#sk":"sk"
            },
            ExpressionAttributeValues:{
                ":send":body.amountset,
                ":sendround":sendround
            },
            ReturnValues:"UPDATED_NEW"
        };
        if(newlist===true){
            params.UpdateExpression = "ADD #send :send SET #sendround = :sendround";
        }
        console.log(JSON.stringify(params));
        docClient.update(params, function(err, data) {
            if (err) {
                if(newlist===true){
                    reject(-1); 
                }else if(err.code==="ValidationException" && err.message==="The provided expression refers to an attribute that does not exist in the item"){
                    addSend(body,periods,userAuthorized,bill,true).then(function(){
                        resolve();  
                    })
                }else{
                    console.log(JSON.stringify(err))
                    reject(-1); 
                }
            } else {
                console.log(JSON.stringify(data))
                resolve();    
            }
        });
    });
}
function addNumber(body,userAuthorized,chkresultconfig){
    return new Promise((resolve, reject) => {
        let priceset = chkresultconfig.priceset;
        let prizepool = chkresultconfig.prizepool;
        let ad = Date.now();
        let params = {
            TableName : TABLE_NUMBER1+"_"+body.periods,
            Item:{
                "owner": userAuthorized.principalId,
                "sk": "a#"+ad+"#"+body.number,
                "number":body.number.toString(),
                "bill":body.bill,
                "createdate": ad,
                "topowner":userAuthorized.toplvl,
                "amountset":body.amountset,
                "priceset":priceset,
                "price":(priceset*body.amountset),
                "periods":body.periods,
                "prizepool":prizepool,
                "confirm":Math.floor((Math.random()*10)%2),
            }
        };
        console.log(params);
        docClient.put(params, function(err, data) {
            if (err) {
                let errMsg = "Unable to put item. Error JSON:"+ JSON.stringify(err, null, 2);
                reject(errMsg);
            } else {
                resolve(ResponseCreate);
                // updateUsedCredit(params.Item,"+").then(()=>{
                //     resolve(ResponseCreate);
                // }).catch(err=>{
                //     reject(err);
                // });
                
            }
        });
    });
}

function getNextWednesday(next=false){
    var d = new Date();
    d.setDate(d.getDate()+ (3 + 7 - d.getDay()) % 7);
    if(next===true){
        d.setDate(d.getDate()+ 1);
        d.setDate(d.getDate()+ (3 + 7 - d.getDay()) % 7);
    }
    return d.getFullYear()+'-'+('0' +(d.getMonth()+1)).slice(-2)+'-'+('0' +(d.getDate()) ).slice(-2)
}
function checkTime(periods){
    let gameEndAtString = periods+"T"+"20:00:00+07:00";
    let gameEndAt = new Date(gameEndAtString);
    let thisTime = new Date();
    console.log("thisTime "+thisTime)
    console.log("gameEndAt "+gameEndAt)
    if(thisTime<gameEndAt){
        return true;
    }else{
        return false;
    }
}
function checkPeriods(userperiods){
    let periods = getNextWednesday()
    if(periods===userperiods){
        console.log('checktime')
        if(checkTime(periods)===true){
            return true;
        }else{
            return false;
        }
    }else{
        return false;
    }
}
function updateBill(userAuthorized){
    return new Promise((resolve, reject) => {
        let periods = getNextWednesday(true)
        let gameEndAtString = periods+"T"+"20:00:00+07:00";
        let gameEndAt = new Date(gameEndAtString);
        let exp= gameEndAt/1000
        let params = {
            TableName : TABLE_USER_EXTEND,
            Key : {
                username : userAuthorized.principalId,
                extend:"number1#bill"
            },
            UpdateExpression: "ADD #bill :bill SET #exp = :exp",
            ExpressionAttributeNames:{
                "#bill":"bill",
                "#exp":"exp"
            },
            ExpressionAttributeValues:{
                ":bill":1,
                ":exp":exp
            },
            ReturnValues:"NONE"
        };
        docClient.update(params, function(err, data) {
            if (err) {
                let errMsg = "Unable to updateBill item. Error JSON:"+ JSON.stringify(err, null, 2);
                reject(errMsg);
            } else {
                resolve();
            }
        });
    });
}