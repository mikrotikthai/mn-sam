const AWS_REGION = process.env.AWS_REGION;
const ACCESS_CONTROL_ALLOW_ORIGIN = process.env.ACCESS_CONTROL_ALLOW_ORIGIN;
const TABLE_USER_EXTEND = process.env.TABLE_USER_EXTEND; 
const TABLE_USER = process.env.TABLE_USER;
const TABLE_GAMERESULT = process.env.TABLE_GAMERESULT;
const SNS_CAL_JACKPOTS = process.env.SNS_CAL_JACKPOTS

const AWS = require("aws-sdk");
const docClient = new AWS.DynamoDB.DocumentClient({region: AWS_REGION});
const sns = new AWS.SNS({region: AWS_REGION,apiVersion: '2010-03-31'});
var ResponseOk = {
    statusCode: 200,
    body:JSON.stringify({"message":"ok"}),
    headers:{
        "Access-Control-Allow-Origin":ACCESS_CONTROL_ALLOW_ORIGIN
    }
};
var Response405 = {
    statusCode: 405,
    body:JSON.stringify({"message":"Method Not Allowed to Update Reuslt"}),
    headers:{
        "Access-Control-Allow-Origin":ACCESS_CONTROL_ALLOW_ORIGIN
    }
    
};
var ResponseNotfound = {
    statusCode: 404,
    body:JSON.stringify({"message":"result not found"}),
    headers:{
        "Access-Control-Allow-Origin":ACCESS_CONTROL_ALLOW_ORIGIN
    }
};

exports.handle = async function(event, context) {
    console.log(JSON.stringify(event));
    let bodyjson = JSON.parse(event.body);
    let userAuthorized = event.requestContext.authorizer;
    try {
        if(event.httpMethod==="GET"){
            return await getResult(event.pathParameters);
        }else if(event.httpMethod==="PATCH"){
            return await updateResult(bodyjson);
        }else{
            console.error("httpMethod not match");
        }
    }catch (error) {
        console.error(error);
        throw new Error(error);
    }
};
function getResult(pathParameters){
    return new Promise(async (resolve, reject) => {
        let params = {
            TableName : TABLE_GAMERESULT,
            Key : {
                "year" : Number(pathParameters.periods.substring(0, 4)),
                "gameperiods" : "number1#"+pathParameters.periods
            },
            ProjectionExpression :"#result,#periods",
            ExpressionAttributeNames:{
                "#result":"result",
                "#periods":"periods"
            }
        };
        console.log(params);
        docClient.get(params, function(err, data) {
            if (err) {
                let errMsg = "Unable to update item. Error JSON:"+ JSON.stringify(err, null, 2);
                console.log(errMsg);
                reject(errMsg);
            } else {
                console.log(data)
                if( !(data.Item === undefined) ){
                    if( !(data.Item.result === undefined) ){
                        resolve({
                            statusCode: 200,
                            body:JSON.stringify(data.Item),
                            headers:{
                                "Access-Control-Allow-Origin":ACCESS_CONTROL_ALLOW_ORIGIN
                            }
                        });
                    }else{
                        resolve(ResponseNotfound);
                    }
                }else{
                    resolve(ResponseNotfound);
                }
            }
        });

    });
}
function updateResult(body){
    return new Promise((resolve, reject) => {
        let thisperiods = getNextWednesday();
        if(thisperiods===body.periods){
            let fullnumber =  body.result.toString();
            let resultset = {
                "4match": fullnumber,
                "4combi": getAnagrams(fullnumber),
                "3match": fullnumber.substring(1, 4),
                "3combi": getAnagrams(fullnumber.substring(1, 4)),
                "2frist": fullnumber.substring(0, 2),
                "2last": fullnumber.substring(2, 4)
            } 
            let params = {
                TableName : TABLE_GAMERESULT,
                Key : {
                    "year" : Number(body.periods.substring(0, 4)),
                    "gameperiods" : "number1#"+body.periods
                },
                ConditionExpression:"attribute_exists(#year) and attribute_exists(gameperiods) and attribute_exists(periods)",
                UpdateExpression:"SET #result = :result , #lastupdate = :lastupdate ,#status = :status",
                ExpressionAttributeNames: { 
                    "#year" : "year",
                    "#result" : "result",
                    "#lastupdate" : "lastupdate",
                    "#status" : "status"
                },
                ExpressionAttributeValues: { 
                    ":result" : resultset,
                    ":lastupdate" : Date.now(),
                    ":status": "END"
                },
                ReturnValues:"UPDATED_OLD"
            };
            docClient.update(params, function(err, data) {
                if (err) {
                    let errMsg = "Unable to put item. Error JSON:"+ JSON.stringify(err, null, 2);
                    reject(errMsg);
                } else {
                    console.log(JSON.stringify(data));
                    let payload;
                    if('result' in data.Attributes){
                        if(data.Attributes.result['4match']===resultset['4match']){
                            //old result
                            console.log("notupdate jackpots old result="+data.Attributes.result['4match']+" new result="+resultset['4match']);
                            resolve(ResponseOk);
                        }else{
                            //update new result
                            console.log("recal jackpots old result="+data.Attributes.result['4match']+" new result="+resultset['4match']);
                            payload = {
                                "resultset":resultset,
                                "periods":body.periods,
                                "recal":true
                            }
                            publishSNS(payload).then(function(){
                                resolve(ResponseOk);
                            }).catch(function(err){
                                resolve(ResponseOk);
                            });
                        }
                    }else{
                        //update result fristtime
                        console.log("update result fristtime="+resultset['4match'])
                        payload = {
                            "resultset":resultset,
                            "periods":body.periods
                        }
                        publishSNS(payload).then(function(){
                            resolve(ResponseOk);
                        }).catch(function(err){
                            resolve(ResponseOk);
                        });
                    }

                }
            });
        }else{
            resolve(Response405);
        }
    });
}
function swap(chars, i, j) {
    var tmp = chars[i];
    chars[i] = chars[j];
    chars[j] = tmp;
}
function getAnagrams(input) {
    var counter = [],
        anagrams = [],
        chars = input.split(''),
        length = chars.length,
        i;

    for (i = 0; i < length; i++) {
        counter[i] = 0;
    }

    //anagrams.push(input);
    i = 0;
    while (i < length) {
        if (counter[i] < i) {
            swap(chars, i % 2 === 1 ? counter[i] : 0, i);
            counter[i]++;
            i = 0;
            anagrams.push(chars.join(''));
        } else {
            counter[i] = 0;
            i++;
        }
    }

    return anagrams;
}
function getNextWednesday(next=false){
    var d = new Date();
    d.setDate(d.getDate()+ (3 + 7 - d.getDay()) % 7);
    if(next===true){
        d.setDate(d.getDate()+ 1);
        d.setDate(d.getDate()+ (3 + 7 - d.getDay()) % 7);
    }
    return d.getFullYear()+'-'+('0' +(d.getMonth()+1)).slice(-2)+'-'+('0' +(d.getDate()) ).slice(-2)
}
function publishSNS(payload){
    return new Promise((resolve, reject) => {
        var params = {
        Message: JSON.stringify(payload),
        TopicArn: SNS_CAL_JACKPOTS
        };
        sns.publish(params, function(err, data) {
            if (err){
                console.log(err, err.stack); // an error occurred
                reject(err);
            }else{
                console.log(data);           // successful response
                resolve();
            }    
        });
    });
}