const AWS_REGION = process.env.AWS_REGION;
const TABLE_NUMBER1 = process.env.TABLE_NUMBER1;
const TABLE_GAMERESULT = process.env.TABLE_GAMERESULT;
const TABLE_NUMBER1_READCAP = process.env.TABLE_NUMBER1_READCAP;
const TABLE_NUMBER1_WRITECAP = process.env.TABLE_NUMBER1_WRITECAP;
const INDEX_NUMBER1_TOP = process.env.INDEX_NUMBER1_TOP;
const INDEX_NUMBER1_TOP_READCAP = process.env.INDEX_NUMBER1_TOP_READCAP;
const INDEX_NUMBER1_TOP_WRITECAP = process.env.INDEX_NUMBER1_TOP_WRITECAP;
const INDEX_NUMBER1_JACKPOTS = process.env.INDEX_NUMBER1_JACKPOTS;
const INDEX_NUMBER1_CONFIRM = process.env.INDEX_NUMBER1_CONFIRM;
const INDEX_NUMBER1_CONFIRM_READCAP = process.env.INDEX_NUMBER1_CONFIRM_READCAP;
const INDEX_NUMBER1_CONFIRM_WRITECAP = process.env.INDEX_NUMBER1_CONFIRM_WRITECAP;
const INDEX_NUMBER1_TOP_JACKPOTS = process.env.INDEX_NUMBER1_TOP_JACKPOTS;
const INDEX_NUMBER1_TOP_JACKPOTS_READCAP = process.env.INDEX_NUMBER1_TOP_JACKPOTS_READCAP;
const INDEX_NUMBER1_TOP_JACKPOTS_WRITECAP = process.env.INDEX_NUMBER1_TOP_JACKPOTS_WRITECAP;


const AWS = require("aws-sdk");
const dynamodb = new AWS.DynamoDB({region: AWS_REGION});
const docClient = new AWS.DynamoDB.DocumentClient({region: AWS_REGION});
exports.handle = function(event, context ,callback) {
    console.log(JSON.stringify(event));
    var periods = getNextWednesday(event.time,true)
    var tableName = TABLE_NUMBER1+"_"+periods;

    var params = {
        TableName : tableName,
        KeySchema: [       
            { AttributeName: "owner", KeyType: "HASH"},  //Partition key
            { AttributeName: "sk", KeyType: "RANGE" }  //Sort key
        ],
        AttributeDefinitions: [       
            { AttributeName: "owner", AttributeType: "S" },
            { AttributeName: "sk", AttributeType: "S" },
            { AttributeName: "topowner", AttributeType: "S" },
            { AttributeName: "confirm", AttributeType: "N" },
            { AttributeName: "jackpots", AttributeType: "S" }
        ],
        ProvisionedThroughput: {       
            ReadCapacityUnits: Number(TABLE_NUMBER1_READCAP), 
            WriteCapacityUnits: Number(TABLE_NUMBER1_WRITECAP)
        },
        GlobalSecondaryIndexes: [
            {
              IndexName: INDEX_NUMBER1_TOP,
              KeySchema: [
                {
                  AttributeName: 'topowner',
                  KeyType: "HASH"
                },
                {
                    AttributeName: 'sk',
                    KeyType: "RANGE"
                }
              ],
              Projection: {
                NonKeyAttributes: [
                  'createdate',
                  'priceset',
                  'bill',
                  'number',
                  'price',
                  'confirm',
                  'amountset'
                ],
                ProjectionType: "INCLUDE"
              },
              ProvisionedThroughput: {
                ReadCapacityUnits: INDEX_NUMBER1_TOP_READCAP,
                WriteCapacityUnits: INDEX_NUMBER1_TOP_WRITECAP 
              }
            },
            {
                IndexName: INDEX_NUMBER1_TOP_JACKPOTS,
                KeySchema: [
                  {
                    AttributeName: 'topowner',
                    KeyType: "HASH"
                  },
                  {
                      AttributeName: 'jackpots',
                      KeyType: "RANGE"
                  }
                ],
                Projection: {
                  ProjectionType: "ALL"
                },
                ProvisionedThroughput: {
                  ReadCapacityUnits: Number(INDEX_NUMBER1_TOP_JACKPOTS_READCAP),
                  WriteCapacityUnits: Number(INDEX_NUMBER1_TOP_JACKPOTS_WRITECAP)
                }
            },
            {
                IndexName: INDEX_NUMBER1_CONFIRM,
                KeySchema: [
                  {
                    AttributeName: 'confirm',
                    KeyType: "HASH"
                  }
                ],
                Projection: {
                  NonKeyAttributes: [
                    'number',
                    'prizepool',
                    'jackpots'
                  ],
                  ProjectionType: "INCLUDE"
                },
                ProvisionedThroughput: {
                  ReadCapacityUnits: Number(INDEX_NUMBER1_CONFIRM_READCAP),
                  WriteCapacityUnits: Number(INDEX_NUMBER1_CONFIRM_WRITECAP)
                }
            }
        ],
        LocalSecondaryIndexes: [
            {
                IndexName: INDEX_NUMBER1_JACKPOTS,
                KeySchema: [
                    { AttributeName: "owner", KeyType: "HASH" },
                    { AttributeName: "jackpots", KeyType: "RANGE" }
                ],
                Projection: {
                    ProjectionType: "ALL"
                }
            }
        ],
        StreamSpecification: {
            StreamEnabled: true,
            StreamViewType: "NEW_AND_OLD_IMAGES"
        }
    };
    
    dynamodb.createTable(params, function(err, data) {
        if(err){
            console.error(JSON.stringify(err));
            callback(JSON.stringify(err))
        }else{
            console.log(JSON.stringify(data));
            putResult(periods,data).then(()=>{
                callback(null,'ok')
            }).catch((err)=>{
                callback(JSON.stringify(err))
            })
        }
    });

};
function getNextWednesday(strdate,next=false){
    var di = Date.parse(strdate)
    var d = new Date(di);
    d.setDate(d.getDate()+ (3 + 7 - d.getDay()) % 7);
    if(next===true){
        d.setDate(d.getDate()+ 1);
        d.setDate(d.getDate()+ (3 + 7 - d.getDay()) % 7);
    }
    return d.getFullYear()+'-'+('0' +(d.getMonth()+1)).slice(-2)+'-'+('0' +(d.getDate()) ).slice(-2)
}

function putResult(periods,resultCreatetable){
    return new Promise(async (resolve, reject) => {
        let params = {
            TableName : TABLE_GAMERESULT,
            Item:{
                "year": Number(periods.substring(0, 4)),
                "gameperiods": "number1#"+periods,
                "status":resultCreatetable.TableDescription.TableStatus,
                "lastupdate":Date.now(),
                "periods":periods
            }
        };
        docClient.put(params, function(err, data) {
            if(err){
                console.error(JSON.stringify(err));
                reject(JSON.stringify(err));
            }else{
                resolve();
            }
            
        });
    });
}