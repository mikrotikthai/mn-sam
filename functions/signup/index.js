const AWS_REGION = process.env.AWS_REGION;
const ACCESS_CONTROL_ALLOW_ORIGIN = process.env.ACCESS_CONTROL_ALLOW_ORIGIN;
const TABLE_USER = process.env.TABLE_USER;

const AWS = require("aws-sdk");
const docClient = new AWS.DynamoDB.DocumentClient({region: AWS_REGION});
var ResponseCreate = {
    statusCode: 201,
    body:JSON.stringify({"message":"ok"}),
    headers:{
        "Access-Control-Allow-Origin":ACCESS_CONTROL_ALLOW_ORIGIN
    }
};
var ResponseBadRequest = {
    statusCode: 400,
    body:JSON.stringify({"message":"Bad Request"}),
    headers:{
        "Access-Control-Allow-Origin":ACCESS_CONTROL_ALLOW_ORIGIN
    }
};
var ResponseDuplicate = {
    statusCode: 409,
    body:JSON.stringify({"message":"username is already exists"}),
    headers:{
        "Access-Control-Allow-Origin":ACCESS_CONTROL_ALLOW_ORIGIN
    }
};

exports.handle = async function(event, context) {
    // console.log(JSON.stringify(event));
    let bodyjson = JSON.parse(event.body);
    let refjson = new Buffer(event.pathParameters.ref, 'base64').toString('ascii');
    let ref = JSON.parse(refjson);
    try {
        if(event.httpMethod==="POST"){
            if( !(ref.ref===undefined) && !(ref.refrole===undefined)) {
                return await addMember(bodyjson,ref);
            }else{
                return ResponseBadRequest;
            }
        }else{
            console.error("httpMethod not match");
        }
    }catch (error) {
        console.error(error);
        throw new Error(error);
    }
};

function addMember(body,ref){
    return new Promise((resolve, reject) => {
        let params = {
            TableName : TABLE_USER,
            ConditionExpression:"attribute_not_exists(username)",
            Item:{
                "username": body.username,
                "password": body.password,
                "role":"member",
                "status":"enable",
                "toplvl":ref.ref,
                "toplvlrole":ref.refrole
            }
        };
        if(body.name  !== undefined && body.name !== ''){
            params.Item.name = body.name;
        }
        if(body.phonenumber  !== undefined && body.phonenumber !== ''){
            params.Item.phonenumber = body.phonenumber;
        }
        if(body.line  !== undefined && body.line !== ''){
            params.Item.line = body.line;
        }
        if(body.bankname  !== undefined && body.bankname !== ''){
            params.Item.bankname = body.bankname;
        }
        if(body.bankaccount  !== undefined && body.bankaccount !== ''){
            params.Item.bankaccount = body.bankaccount;
        }
        docClient.put(params, function(err, data) {
            if (err) {
                if(!(err.code === undefined) && err.code === "ConditionalCheckFailedException"){
                    resolve(ResponseDuplicate);
                }else{
                    let errMsg = "Unable to read item. Error JSON:"+ JSON.stringify(err, null, 2);
                    reject(errMsg);
                }
            } else {
                resolve(ResponseCreate);
            }
        });
    });
}