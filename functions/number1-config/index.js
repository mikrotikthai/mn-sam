const AWS_REGION = process.env.AWS_REGION;
const ACCESS_CONTROL_ALLOW_ORIGIN = process.env.ACCESS_CONTROL_ALLOW_ORIGIN;
const TABLE_USER_EXTEND = process.env.TABLE_USER_EXTEND; 
const TABLE_USER = process.env.TABLE_USER;

const AWS = require("aws-sdk");
const docClient = new AWS.DynamoDB.DocumentClient({region: AWS_REGION});
var ResponseOk = {
    statusCode: 200,
    body:JSON.stringify({"message":"ok"}),
    headers:{
        "Access-Control-Allow-Origin":ACCESS_CONTROL_ALLOW_ORIGIN
    }
};
var Response400 = {
    statusCode: 400,
    body:JSON.stringify({"message":"Invalid request"}),
    headers:{
        "Access-Control-Allow-Origin":ACCESS_CONTROL_ALLOW_ORIGIN
    }
};
var ResponseNotfound = {
    statusCode: 404,
    body:JSON.stringify({"message":"config not found"}),
    headers:{
        "Access-Control-Allow-Origin":ACCESS_CONTROL_ALLOW_ORIGIN
    }
};

exports.handle = async function(event, context) {
    console.log(JSON.stringify(event));
    let userAuthorized = event.requestContext.authorizer;
    try {
        if(event.httpMethod==="GET"){
            return await getConfig(userAuthorized,event.queryStringParameters);
        }else if(event.httpMethod==="PUT"){
            let bodyjson = JSON.parse(event.body);
            return await updateConfig(userAuthorized,bodyjson);
        }else if(event.httpMethod==="PATCH"){
            let bodyjson = JSON.parse(event.body);
            return await updateConfig2(userAuthorized,bodyjson);
        }if(event.httpMethod==="DELETE"){
            return await clearConfig(userAuthorized,event.queryStringParameters);
        }else{
            console.error("httpMethod not match");
        }
    }catch (error) {
        console.error(error);
        throw new Error(error);
    }
};

function getConfig(userAuthorized,queryStringParameters){
    return new Promise(async (resolve, reject) => {
        let global,username,summary;
        if(queryStringParameters===null){
            let topuser = await checkTopline(userAuthorized.principalId)
            if(topuser===false){
                resolve(Response400);
            }else{
                getMultiConfig(topuser,'no',userAuthorized.principalId,'only').then(function(result){
                    resolve({
                        statusCode: 200,
                        body:JSON.stringify(result),
                        headers:{
                            "Access-Control-Allow-Origin":ACCESS_CONTROL_ALLOW_ORIGIN
                        }
                    })
                }).catch(function(err){
                    resolve(Response400);
                });
            }
        }else{
            if(userAuthorized.role==="member"){
                resolve(Response400);
            }else{
                if(queryStringParameters.global===undefined){
                    global = 'no';
                }else{
                    global = queryStringParameters.global;
                }
                if(queryStringParameters.username===undefined){
                    username = false;
                }else{
                    username = queryStringParameters.username;
                }
                if(queryStringParameters.summary===undefined){
                    summary = 'no';
                }else{
                    summary = queryStringParameters.summary;
                }
                if(global==='yes' || !(username === false)){
                    getMultiConfig(userAuthorized.principalId,global,username,summary).then(function(result){
                        resolve({
                            statusCode: 200,
                            body:JSON.stringify(result),
                            headers:{
                                "Access-Control-Allow-Origin":ACCESS_CONTROL_ALLOW_ORIGIN
                            }
                        })
                    }).catch(function(err){
                        resolve(Response400);
                    });
                }else{
                    resolve(Response400);
                }
            }
        }
    });
}
function getMultiConfig(topuser,global,username,summary){
    return new Promise(async (resolve, reject) => {
        let params = {
            RequestItems: {}
        };
        let result = {};
        params.RequestItems[TABLE_USER_EXTEND] = {Keys: []};

        if(global==='yes'|| (!(username===false) && (summary==='yes'||summary==='only') )){
            result.global = {};
            params.RequestItems[TABLE_USER_EXTEND].Keys.push({
                username:topuser,
                extend:"number1#gameconfig#global"
            });
        }
        if(!(username === false)){
            let isdown = await checkDownline(username,topuser);
            if(isdown===true){
                result.indi = {};
                params.RequestItems[TABLE_USER_EXTEND].Keys.push({
                    username:username,
                    extend:"number1#gameconfig#indi"
                });
            }else{
                reject();
            }
        }
        docClient.batchGet(params, function(err, data) {
            if (err){
                let errMsg = "Unable to getconfig items. Error JSON:"+ JSON.stringify(err, null, 2);
                console.log(errMsg);
                reject(errMsg);
            }else {
                console.log(JSON.stringify(data));
                let res = data.Responses;
                if( res[TABLE_USER_EXTEND].length === 0){
                    if(summary==='yes' || summary==='only'){
                        result.summary = {}
                        Object.assign(result.summary, result.global)
                        Object.assign(result.summary, result.indi)
                    }
                    if(global==='no'){
                        delete result.global;
                    }
                    if(summary==='only'){
                        delete result.indi;
                    }
                    resolve(result);
                }else{
                    for(var i = 0; i< res[TABLE_USER_EXTEND].length ; i++){
                        if(res[TABLE_USER_EXTEND][i].extend==="number1#gameconfig#global" && 
                        res[TABLE_USER_EXTEND][i].username===topuser
                        ){
                            delete res[TABLE_USER_EXTEND][i].username;
                            delete res[TABLE_USER_EXTEND][i].extend;
                            result.global = res[TABLE_USER_EXTEND][i];
                        }else if(res[TABLE_USER_EXTEND][i].extend==="number1#gameconfig#indi"){
                            delete res[TABLE_USER_EXTEND][i].username;
                            delete res[TABLE_USER_EXTEND][i].extend;
                            result.indi = res[TABLE_USER_EXTEND][i];
                        }
                    }
                    if(summary==='yes' || summary==='only'){
                        result.summary = {}
                        Object.assign(result.summary, result.global)
                        Object.assign(result.summary, result.indi)
                    }
                    if(global==='no'){
                        delete result.global;
                    }
                    if(summary==='only'){
                        delete result.indi;
                    }
                    resolve(result);
                }
            }
        });
    });
}
function updateConfig(userAuthorized,body){
    return new Promise(async (resolve, reject) => {
        let params = {
            RequestItems: {}
        };
        let result = {};
        params.RequestItems[TABLE_USER_EXTEND] = [];
        if('global' in body){
            if(userAuthorized.role==='member'){
                resolve(Response400);
            }else{
                let putitem = {}
                Object.assign(putitem, body.global)
                putitem.username = userAuthorized.principalId;
                putitem.extend = "number1#gameconfig#global";
                params.RequestItems[TABLE_USER_EXTEND].push({
                    PutRequest: {
                        Item: putitem
                    }
                });
            }
        }
        if('indi' in body){
            if(!('username' in body.indi)){
                resolve(Response400)
            }
            let isdown = await checkDownline(body.indi.username,userAuthorized.principalId);
            if(isdown===true){
                let putitem = {}
                Object.assign(putitem, body.indi)
                putitem.username = body.indi.username;
                putitem.extend = "number1#gameconfig#indi";
                params.RequestItems[TABLE_USER_EXTEND].push({
                    PutRequest: {
                        Item: putitem
                    }
                });
            }else{
                resolve(Response400);
            }
        }
        docClient.batchWrite(params, function(err, data) {
            if (err) {
                let errMsg = "Unable to update item. Error JSON:"+ JSON.stringify(err, null, 2);
                console.log(errMsg);
                reject(errMsg);
            }else{
                //console.log(data);
                resolve(ResponseOk)
            }
        });
    });
}
function updateConfig2(userAuthorized,body){
    return new Promise(async (resolve, reject) => {
        if(!('global' in body)){
            resolve(Response400);
        }
        let globalconf = body.global;
        let UpdateStr = "set";
        let AtbValues = {};
        let AtbNames = {}; 
        if('status' in globalconf){
            AtbNames["#status"] = "status";
            AtbValues[":status"] = globalconf.status;
            if(UpdateStr==="set"){
                UpdateStr+=" ";
            }else{
                UpdateStr+=" ,";
            }
            UpdateStr+=" #status = :status";
        }
        if('closetime' in globalconf){
            AtbNames["#closetime"] = "closetime";
            AtbValues[":closetime"] = globalconf.closetime;
            if(UpdateStr==="set"){
                UpdateStr+=" ";
            }else{
                UpdateStr+=" ,";
            }
            UpdateStr+=" #closetime = :closetime";
        }
        if('priceset' in globalconf){
            AtbNames["#priceset"] = "priceset";
            AtbValues[":priceset"] = globalconf.priceset;
            if(UpdateStr==="set"){
                UpdateStr+=" ";
            }else{
                UpdateStr+=" ,";
            }
            UpdateStr+=" #priceset = :priceset";
        }
        if('prizepool' in globalconf){
            AtbNames["#prizepool"] = "prizepool";
            AtbValues[":prizepool"] = globalconf.prizepool;
            if(UpdateStr==="set"){
                UpdateStr+=" ";
            }else{
                UpdateStr+=" ,";
            }
            UpdateStr+=" #prizepool = :prizepool";
        }
        if('limitall' in globalconf){
            AtbNames["#limitall"] = "limitall";
            AtbValues[":limitall"] = globalconf.limitall;
            if(UpdateStr==="set"){
                UpdateStr+=" ";
            }else{
                UpdateStr+=" ,";
            }
            UpdateStr+=" #limitall = :limitall";
        }
        if('limitbynumber' in globalconf){
            AtbNames["#limitbynumber"] = "limitbynumber";
            AtbValues[":limitbynumber"] = globalconf.limitbynumber;
            if(UpdateStr==="set"){
                UpdateStr+=" ";
            }else{
                UpdateStr+=" ,";
            }
            UpdateStr+=" #limitbynumber = :limitbynumber";
        }
        let params = {
            TableName : TABLE_USER_EXTEND,
            Key : {
                username : userAuthorized.principalId,
                extend:"number1#gameconfig#global"
            },
            UpdateExpression: UpdateStr,
            ExpressionAttributeValues:AtbValues,
            ExpressionAttributeNames:AtbNames,
            ReturnValues:"NONE"
        };
        docClient.update(params, function(err, data) {
            if (err) {
                let errMsg = "Unable to update item. Error JSON:"+ JSON.stringify(err, null, 2);
                console.log(errMsg);
                reject(errMsg);
            }else{
                //console.log(data);
                resolve(ResponseOk)
            }
        });
    });
}
function checkDownline(down,top){
    return new Promise((resolve, reject) => {
        let params = {
            TableName : TABLE_USER,
            Key : {
                username : down
            }
        };
        docClient.get(params, function(err, data) {
            if (err) {
                let errMsg = "Unable to read item. Error JSON:"+ JSON.stringify(err, null, 2);
                reject(errMsg);
            } else {
                if(data.Item === undefined){
                    resolve(false);
                }else{
                    if(data.Item.toplvl===top){
                        resolve(true);
                    }else{
                        resolve(false);
                    }
                }
                
                   
            }
        });
    });
}
function checkTopline(user){
    return new Promise((resolve, reject) => {
        let params = {
            TableName : TABLE_USER,
            Key : {
                username : user
            }
        };
        docClient.get(params, function(err, data) {
            if (err) {
                let errMsg = "Unable to read item. Error JSON:"+ JSON.stringify(err, null, 2);
                reject(errMsg);
            } else {
                if(data.Item === undefined){
                    resolve(false);
                }else{
                    resolve(data.Item.toplvl);
                }
                
                   
            }
        });
    });
}
function clearConfig(userAuthorized,queryStringParameters){
    return new Promise(async (resolve, reject) => {
        if(userAuthorized.role==="member" || queryStringParameters===null){
            resolve(Response400);
        }else{
            let params = {
                RequestItems: {}
            };
            params.RequestItems[TABLE_USER_EXTEND] = [];
            if( !(queryStringParameters.global===undefined) && queryStringParameters.global==='yes') {
                let delitem = {}
                delitem.username = userAuthorized.principalId;
                delitem.extend = "number1#gameconfig#global";
                params.RequestItems[TABLE_USER_EXTEND].push({
                    DeleteRequest: {
                        Key: delitem
                    }
                });
            }
            if( !(queryStringParameters.username===undefined)) {
                let isdown = await checkDownline(queryStringParameters.username,userAuthorized.principalId);
                if(isdown===true){
                    let delitem = {}
                    delitem.username = queryStringParameters.username;
                    delitem.extend = "number1#gameconfig#indi";
                    params.RequestItems[TABLE_USER_EXTEND].push({
                        DeleteRequest: {
                            Key: delitem
                        }
                    });
                }else{
                    resolve(Response400);
                }
            }
            console.log(params)
            docClient.batchWrite(params, function(err, data) {
                if (err) {
                    let errMsg = "Unable to update item. Error JSON:"+ JSON.stringify(err, null, 2);
                    console.log(errMsg);
                    reject(errMsg);
                }else{
                    //console.log(data);
                    resolve(ResponseOk)
                }
            });
        }
    });
}