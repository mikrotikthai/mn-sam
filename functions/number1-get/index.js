const AWS_REGION = process.env.AWS_REGION;
const ACCESS_CONTROL_ALLOW_ORIGIN = process.env.ACCESS_CONTROL_ALLOW_ORIGIN;
const TABLE_NUMBER1 = process.env.TABLE_NUMBER1;
const INDEX_NUMBER1_DOWN = process.env.INDEX_NUMBER1_DOWN;
const INDEX_NUMBER1_TOP = process.env.INDEX_NUMBER1_TOP;
const TABLE_USER = process.env.TABLE_USER;
const TABLE_USER_EXTEND = process.env.TABLE_USER_EXTEND; 

const AWS = require("aws-sdk");
const docClient = new AWS.DynamoDB.DocumentClient({region: AWS_REGION});

var ResponseBadRequest = {
    statusCode: 400,
    body:JSON.stringify({"message":"Bad Request"}),
    headers:{
        "Access-Control-Allow-Origin":ACCESS_CONTROL_ALLOW_ORIGIN
    }
};

exports.handle = async function(event, context) {
    let queryStringParameters = event.queryStringParameters;
    let userAuthorized = event.requestContext.authorizer;
    try {
        if(event.httpMethod==="GET" && (queryStringParameters.get==="all" || queryStringParameters.get==="bill")){
            if(queryStringParameters.startkey===undefined){
                return await getAll(userAuthorized,queryStringParameters);
            }else{
                let startkeyjson = new Buffer(queryStringParameters.startkey, 'base64').toString('ascii');
                let ExclusiveStartKey = JSON.parse(startkeyjson);
                return await getAll(userAuthorized,queryStringParameters,ExclusiveStartKey);
            }
        }else if(event.httpMethod==="GET" && queryStringParameters.get==="sum"){
            if(queryStringParameters.startkey===undefined){
                return await getSum(userAuthorized,queryStringParameters);
            }else{
                let startkeyjson = new Buffer(queryStringParameters.startkey, 'base64').toString('ascii');
                let ExclusiveStartKey = JSON.parse(startkeyjson);
                return await getSum(userAuthorized,queryStringParameters,ExclusiveStartKey);
            }
        }else{
            console.error("httpMethod not match");
        }
    }catch (error) {
        console.error(error);
        throw new Error(error);
    }
};

function getAll(userAuthorized,queryStringParameters,ExclusiveStartKey=null){
    return new Promise(async (resolve, reject) => {
        let beginswith = "a#";
        let params = {
            TableName : TABLE_NUMBER1+"_"+queryStringParameters.periods,
            Limit : Number(queryStringParameters.limit),
            ProjectionExpression :"#owner,#number,topowner,bill,createdate,amountset,priceset,price,confirm"
        };
        if((queryStringParameters.get==="bill") && !(queryStringParameters.bill===undefined)){
            beginswith+=queryStringParameters.bill+"#"
        }
        if(queryStringParameters.scope === "top" && Number.isInteger(Number(queryStringParameters.limit))){
            params.KeyConditionExpression = "#owner = :owner and begins_with(sk,:a)";
            params.ExpressionAttributeValues = {
                ":owner": userAuthorized.principalId,
                ":a": beginswith
            };
            params.ExpressionAttributeNames = {
                "#owner": "owner",
                "#number": "number"
            };
        }else if(queryStringParameters.scope === "down" && Number.isInteger(Number(queryStringParameters.limit))){
            params.IndexName = INDEX_NUMBER1_TOP;
            params.KeyConditionExpression = "#topowner = :topowner and begins_with(sk,:a)";
            params.ExpressionAttributeValues = {
                ":topowner": userAuthorized.principalId,
                ":a": beginswith
            };
            params.ExpressionAttributeNames = {
                "#topowner": "topowner",
                "#owner": "owner",
                "#number": "number"
            };
        }else{
            resolve(ResponseBadRequest);
        }

        if(!(ExclusiveStartKey===null)){
            params.ExclusiveStartKey = ExclusiveStartKey;
        }
        if(!(queryStringParameters.scanforward === undefined) && queryStringParameters.scanforward==="no"){
            params.ScanIndexForward = false;
        }else{
            params.ScanIndexForward = true;
        }
        console.log(params);
        docClient.query(params, function(err, data) {
            if (err) {
                if(err.code === "ResourceNotFoundException"){
                    resolve(ResponseBadRequest);
                }else{
                    let errMsg = "Unable to getall item. Error JSON:"+ JSON.stringify(err, null, 2);
                    console.log(errMsg);
                    reject(errMsg);
                }
            } else {
                delete data.ScannedCount;
                if(!(data.LastEvaluatedKey===undefined)){
                    let Lk = data.LastEvaluatedKey;
                    let Lkjson = JSON.stringify(Lk);
                    let newLk = new Buffer(Lkjson).toString('base64');
                    data.LastEvaluatedKey = newLk;
                }
                resolve({
                    statusCode: 200,
                    headers:{
                        "Access-Control-Allow-Origin":ACCESS_CONTROL_ALLOW_ORIGIN
                    },
                    body:JSON.stringify(data)
                });    
            }
        });
    });
}

function getSum(userAuthorized,queryStringParameters,ExclusiveStartKey=null){
    return new Promise(async (resolve, reject) => {
        let params = {
            TableName : TABLE_NUMBER1+"_"+queryStringParameters.periods,
            Limit : Number(queryStringParameters.limit),
            ProjectionExpression :"#owner,#number,topowner,bill,createdate,amountset,priceset,price,confirm"
        };
        if(queryStringParameters.scope === "down" && Number.isInteger(Number(queryStringParameters.limit))){
            params.KeyConditionExpression = "#owner = :owner and begins_with(sk,:s)";
            params.ExpressionAttributeValues = {
                ":owner": userAuthorized.principalId,
                ":s": "s#"
            };
            params.ExpressionAttributeNames = {
                "#owner": "owner",
                "#number": "number"
            };
        }else{
            resolve(ResponseBadRequest);
        }

        if(!(ExclusiveStartKey===null)){
            params.ExclusiveStartKey = ExclusiveStartKey;
        }
        console.log(params);
        docClient.query(params, function(err, data) {
            if (err) {
                if(err.code === "ResourceNotFoundException"){
                    resolve(ResponseBadRequest);
                }else{
                    let errMsg = "Unable to getall item. Error JSON:"+ JSON.stringify(err, null, 2);
                    console.log(errMsg);
                    reject(errMsg);
                }
            } else {
                delete data.ScannedCount;
                if(!(data.LastEvaluatedKey===undefined)){
                    let Lk = data.LastEvaluatedKey;
                    let Lkjson = JSON.stringify(Lk);
                    let newLk = new Buffer(Lkjson).toString('base64');
                    data.LastEvaluatedKey = newLk;
                }
                resolve({
                    statusCode: 200,
                    headers:{
                        "Access-Control-Allow-Origin":ACCESS_CONTROL_ALLOW_ORIGIN
                    },
                    body:JSON.stringify(data)
                });    
            }
        });
    });
}