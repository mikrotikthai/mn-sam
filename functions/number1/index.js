const AWS_REGION = process.env.AWS_REGION;
const ACCESS_CONTROL_ALLOW_ORIGIN = process.env.ACCESS_CONTROL_ALLOW_ORIGIN;
const TABLE_NUMBER1 = process.env.TABLE_NUMBER1;
const TABLE_USER_EXTEND = process.env.TABLE_USER_EXTEND; 

const AWS = require("aws-sdk");
const docClient = new AWS.DynamoDB.DocumentClient({region: AWS_REGION});
var ResponseCreate = {
    statusCode: 201,
    body:JSON.stringify({"message":"ok"}),
    headers:{
        "Access-Control-Allow-Origin":ACCESS_CONTROL_ALLOW_ORIGIN
    }
};
var ResponseOk = {
    statusCode: 200,
    body:JSON.stringify({"message":"ok"}),
    headers:{
        "Access-Control-Allow-Origin":ACCESS_CONTROL_ALLOW_ORIGIN
    }
};
var ResponseConfirmError = {
    statusCode: 400,
    body:JSON.stringify({"message":"invalid periods"}),
    headers:{
        "Access-Control-Allow-Origin":ACCESS_CONTROL_ALLOW_ORIGIN
    }
};
var ResponseBadRequest = {
    statusCode: 400,
    body:JSON.stringify({"message":"Bad Request"}),
    headers:{
        "Access-Control-Allow-Origin":ACCESS_CONTROL_ALLOW_ORIGIN
    }
};
var ResponseNotAllow = {// not have credit
    statusCode: 405,
    body:JSON.stringify({"message":"Method Not Allowed"}),
    headers:{
        "Access-Control-Allow-Origin":ACCESS_CONTROL_ALLOW_ORIGIN
    }
};
var ResponseNotAccept = {// missing config
    statusCode: 406,
    body:JSON.stringify({"message":"Game close or Missing config"}),
    headers:{
        "Access-Control-Allow-Origin":ACCESS_CONTROL_ALLOW_ORIGIN
    }
};
var ResponseGameEnd = {// game end
    statusCode: 410,
    body:JSON.stringify({"message":"Game is end"}),
    headers:{
        "Access-Control-Allow-Origin":ACCESS_CONTROL_ALLOW_ORIGIN
    }
};

exports.handle = async function(event, context) {
    // console.log(JSON.stringify(event));
    let bodyjson = JSON.parse(event.body);
    let userAuthorized = event.requestContext.authorizer;
    try {
        if(event.httpMethod==="POST"){
            if(checkPeriods(bodyjson.periods)===true){
                if(bodyjson.number.toString().length===4){
                    let chkresult = await getConfig(userAuthorized,bodyjson);
                    if(chkresult.status===true){
                        return await addNumber(bodyjson,userAuthorized,chkresult);
                    }else{
                        if(chkresult.code == "406"){
                            return ResponseNotAccept;
                        }else{
                            return ResponseNotAllow;
                        }
                    }
                }else{
                    return ResponseBadRequest;
                }
            }else{
                return ResponseGameEnd;
            }
        }else if(event.httpMethod==="PATCH"){
            if(checkPeriods(bodyjson.periods)===true){
                let chkresult = await getConfig2(userAuthorized,bodyjson);
                if(chkresult.status===true){
                    return await comfirmNumber(bodyjson,userAuthorized);
                }else{
                    if(chkresult.code == "406"){
                        return ResponseNotAccept;
                    }else{
                        return ResponseNotAllow;
                    }
                }
            }else{
                return ResponseGameEnd;
            }
        }else if(event.httpMethod==="DELETE"){
            return await delNumber(bodyjson,userAuthorized);
        }else{
            console.error("httpMethod not match");
        }
    }catch (error) {
        console.error(error);
        throw new Error(error);
    }
};
function getConfig(userAuthorized,body){
    return new Promise((resolve, reject) => {
        let TableNum = TABLE_NUMBER1+"_"+body.periods;
        let params = {
            RequestItems: {}
        };
        params.RequestItems[TABLE_USER_EXTEND] ={
            Keys: [
                {
                    username:userAuthorized.toplvl,
                    extend:"number1#gameconfig#global"
                },
                {
                    username:userAuthorized.principalId,
                    extend:"number1#gameconfig#indi"
                },
                {
                    username:userAuthorized.principalId,
                    extend:"number1#credit"
                },
                {
                    username:userAuthorized.principalId,
                    extend:"number1#bill"
                }
            ]
        }
        params.RequestItems[TableNum]= {
            Keys: [
                {
                    owner:userAuthorized.principalId,
                    sk:"sumadd"
                },
            ]
        }
        docClient.batchGet(params, function(err, data) {
            let result = {};
            if (err){
                if(err.code === "ResourceNotFoundException"){
                    result.status = false;
                    result.code = "406";
                    resolve(result);
                }else{
                    let errMsg = "Unable to getconfig items. Error JSON:"+ JSON.stringify(err, null, 2);
                    console.log(errMsg);
                    reject(errMsg);
                }
                
            }else {
                console.log(JSON.stringify(data));
                let res = data.Responses;
                if( res[TABLE_USER_EXTEND].length === 0){
                    result.status = false;
                    result.code = "406";
                    resolve(result);
                }else{
                    let config = {};
                    for(var i = 0; i< res[TABLE_USER_EXTEND].length ; i++){
                        if(res[TABLE_USER_EXTEND][i].extend==="number1#gameconfig#global"){
                            if(!(res[TABLE_USER_EXTEND][i].priceset===undefined) && !('priceset' in config) ){
                                config.priceset = res[TABLE_USER_EXTEND][i].priceset;
                            }
                            if(!(res[TABLE_USER_EXTEND][i].status===undefined) && !('status' in config)){
                                config.status = res[TABLE_USER_EXTEND][i].status;
                            }
                            if(!(res[TABLE_USER_EXTEND][i].closetime===undefined) && !('closetime' in config)){
                                config.closetime = res[TABLE_USER_EXTEND][i].closetime;
                            }  
                            if(!(res[TABLE_USER_EXTEND][i].prizepool===undefined) && !('prizepool' in config)){
                                config.prizepool = res[TABLE_USER_EXTEND][i].prizepool;
                            }   
                        }else if(res[TABLE_USER_EXTEND][i].extend==="number1#gameconfig#indi"){
                            if(!(res[TABLE_USER_EXTEND][i].priceset===undefined)){
                                config.priceset = res[TABLE_USER_EXTEND][i].priceset;
                            }
                            if(!(res[TABLE_USER_EXTEND][i].status===undefined)){
                                config.status = res[TABLE_USER_EXTEND][i].status;
                            }
                            if(!(res[TABLE_USER_EXTEND][i].closetime===undefined)){
                                config.closetime = res[TABLE_USER_EXTEND][i].closetime;
                            }
                            if(!(res[TABLE_USER_EXTEND][i].prizepool===undefined)){
                                config.prizepool = res[TABLE_USER_EXTEND][i].prizepool;
                            }   
                        }else if(res[TABLE_USER_EXTEND][i].extend==="number1#credit"){
                            config.credit = res[TABLE_USER_EXTEND][i].credit;
                        }else if(res[TABLE_USER_EXTEND][i].extend==="number1#bill"){
                            config.bill = res[TABLE_USER_EXTEND][i].bill;
                        }
                    }
                    if(config.bill===undefined){
                        result.bill = 0;
                    }else{
                        result.bill = config.bill;
                    }
                    console.log(config)
                    if( !(config.status===undefined) && (config.status==="open") ){
                        if( !(config.closetime===undefined) 
                            && (config.closetime>Date.now()) 
                            && !(config.prizepool===undefined) 
                            ){
                            result.prizepool = config.prizepool
                            if(config.priceset === undefined){
                                result.priceset = 120;
                            }else{
                                result.priceset = config.priceset;
                            }
                            let usedcredit = 0;
                            if(res[TableNum].length === 0){
                                usedcredit = 0
                            }else{
                                usedcredit = res[TableNum][0].price;
                            }
                            console.log("usedcredit "+usedcredit);
                            if( (usedcredit+(result.priceset*body.amountset) ) <= config.credit){
                                result.status = true;
                                resolve(result);
                            }else{
                                result.status = false;
                                result.code = "405";
                                resolve(result);
                            }
                        }else{
                            result.status = false;
                            result.code = "406";
                            resolve(result);
                        }
                    }else{
                        result.status = false;
                        result.code = "406";
                        resolve(result);
                    }
                    
                }
            }
        });
    });
}
function addNumber(body,userAuthorized,chkresultconfig){
    return new Promise((resolve, reject) => {
        let priceset = chkresultconfig.priceset;
        let bill = chkresultconfig.bill;
        let prizepool = chkresultconfig.prizepool;
        let ad = Date.now();
        let params = {
            TableName : TABLE_NUMBER1+"_"+body.periods,
            Item:{
                "owner": userAuthorized.principalId,
                "sk": "a#"+body.bill+"#"+ad+"#"+body.number,
                "number":body.number.toString(),
                "bill":body.bill,
                "createdate": ad,
                "topowner":userAuthorized.toplvl,
                "amountset":body.amountset,
                "priceset":priceset,
                "price":(priceset*body.amountset),
                "periods":body.periods,
                "prizepool":prizepool
            }
        };
        console.log(params);
        if( (params.Item.price === body.price) && (priceset===body.priceset) && (bill===body.bill)){
            docClient.put(params, function(err, data) {
                if (err) {
                    let errMsg = "Unable to put item. Error JSON:"+ JSON.stringify(err, null, 2);
                    reject(errMsg);
                } else {
                    resolve(ResponseCreate);
                    // updateUsedCredit(params.Item,"+").then(()=>{
                    //     resolve(ResponseCreate);
                    // }).catch(err=>{
                    //     reject(err);
                    // });
                    
                }
            });
        }else{
            resolve(ResponseBadRequest);
        }
    });
}
function getBill(userAuthorized){
    return new Promise((resolve, reject) => {
        let params = {
            TableName : TABLE_USER_EXTEND,
            Key : {
                username : userAuthorized.principalId,
                extend:"number1#bill"
            }
        };
        docClient.get(params, function(err, data) {
            if (err) {
                let errMsg = "Unable to getBill item. Error JSON:"+ JSON.stringify(err, null, 2);
                reject(errMsg);
            } else {
                if(data.Item===undefined){
                    resolve(0);
                }else{
                    resolve(data.Item.bill);
                }
                
            }
        });
    });
}
function updateBill(userAuthorized,exp=Date.now()+3600){
    return new Promise((resolve, reject) => {
        let periods = getNextWednesday(true)
        let gameEndAtString = periods+"T"+"20:00:00+07:00";
        let gameEndAt = new Date(gameEndAtString);
        let exp= gameEndAt/1000
        let params = {
            TableName : TABLE_USER_EXTEND,
            Key : {
                username : userAuthorized.principalId,
                extend:"number1#bill"
            },
            UpdateExpression: "ADD #bill :bill SET #exp = :exp",
            ExpressionAttributeNames:{
                "#bill":"bill",
                "#exp":"exp"
            },
            ExpressionAttributeValues:{
                ":bill":1,
                ":exp":exp
            },
            ReturnValues:"NONE"
        };
        docClient.update(params, function(err, data) {
            if (err) {
                let errMsg = "Unable to updateBill item. Error JSON:"+ JSON.stringify(err, null, 2);
                reject(errMsg);
            } else {
                resolve();
            }
        });
    });
}
function getConfig2(userAuthorized,body){
    return new Promise((resolve, reject) => {
        let params = {
            RequestItems: {}
        };
        params.RequestItems[TABLE_USER_EXTEND] ={
            Keys: [
                {
                    username:userAuthorized.toplvl,
                    extend:"number1#gameconfig#global"
                },
                {
                    username:userAuthorized.principalId,
                    extend:"number1#gameconfig#indi"
                }
            ]
        }
        docClient.batchGet(params, function(err, data) {
            let result = {};
            if (err){
                if(err.code === "ResourceNotFoundException"){
                    result.status = false;
                    result.code = "406";
                    resolve(result);
                }else{
                    let errMsg = "Unable to getconfig items. Error JSON:"+ JSON.stringify(err, null, 2);
                    console.log(errMsg);
                    reject(errMsg);
                }
                
            }else {
                console.log(JSON.stringify(data));
                let res = data.Responses;
                if( res[TABLE_USER_EXTEND].length === 0){
                    result.status = false;
                    result.code = "406";
                    resolve(result);
                }else{
                    let config = {};
                    for(var i = 0; i< res[TABLE_USER_EXTEND].length ; i++){
                        if(res[TABLE_USER_EXTEND][i].extend==="number1#gameconfig#global"){
                            if(!(res[TABLE_USER_EXTEND][i].priceset===undefined) && !('priceset' in config) ){
                                config.priceset = res[TABLE_USER_EXTEND][i].priceset;
                            }
                            if(!(res[TABLE_USER_EXTEND][i].status===undefined) && !('status' in config)){
                                config.status = res[TABLE_USER_EXTEND][i].status;
                            }
                            if(!(res[TABLE_USER_EXTEND][i].closetime===undefined) && !('closetime' in config)){
                                config.closetime = res[TABLE_USER_EXTEND][i].closetime;
                            }  
                            if(!(res[TABLE_USER_EXTEND][i].prizepool===undefined) && !('prizepool' in config)){
                                config.prizepool = res[TABLE_USER_EXTEND][i].prizepool;
                            }   
                        }else if(res[TABLE_USER_EXTEND][i].extend==="number1#gameconfig#indi"){
                            if(!(res[TABLE_USER_EXTEND][i].priceset===undefined)){
                                config.priceset = res[TABLE_USER_EXTEND][i].priceset;
                            }
                            if(!(res[TABLE_USER_EXTEND][i].status===undefined)){
                                config.status = res[TABLE_USER_EXTEND][i].status;
                            }
                            if(!(res[TABLE_USER_EXTEND][i].closetime===undefined)){
                                config.closetime = res[TABLE_USER_EXTEND][i].closetime;
                            }
                            if(!(res[TABLE_USER_EXTEND][i].prizepool===undefined)){
                                config.prizepool = res[TABLE_USER_EXTEND][i].prizepool;
                            }   
                        }
                    }
                    console.log(config)
                    if( !(config.status===undefined) && (config.status==="open") ){
                        if( !(config.closetime===undefined) 
                            && (config.closetime>Date.now()) 
                            && !(config.prizepool===undefined) 
                            ){
                                result.status = true;
                                resolve(result);
                        }else{
                            result.status = false;
                            result.code = "406";
                            resolve(result);
                        }
                    }else{
                        result.status = false;
                        result.code = "406";
                        resolve(result);
                    }
                    
                }
            }
        });
    });
}
function comfirmNumber(bodyAll,userAuthorized){
    return new Promise(async (resolve, reject) => {
        var allresult = [];
        try {
            let mlimit = await getLimit(userAuthorized);
            let bill = await getBill(userAuthorized);
            var count = 0;
            var msglist = [];
            var countNConf = {};
            for(var i=0;i<bodyAll.confirm.length;i++){
                let resultOne =  await comfirmOnce(bodyAll.confirm[i],userAuthorized,bodyAll.periods,bill,mlimit,countNConf);
                if(resultOne === "confirmed"){
                    if(bodyAll.confirm[i].number.toString() in countNConf){
                        countNConf[bodyAll.confirm[i].number.toString()] += bodyAll.confirm[i].amountset
                    }else{
                        countNConf[bodyAll.confirm[i].number.toString()] = bodyAll.confirm[i].amountset
                    }
                    count++;
                }else{
                    msglist.push(resultOne);
                }
            }
            if(count>0){
                await updateBill(userAuthorized);
            }
            resolve({
                statusCode: 200,
                body:JSON.stringify({"confirmed":count,"error":msglist}),
                headers:{
                    "Access-Control-Allow-Origin":ACCESS_CONTROL_ALLOW_ORIGIN
                }
            });

        }catch (error) {
            resolve({
                statusCode: 400,
                body:JSON.stringify({"message":error}),
                headers:{
                    "Access-Control-Allow-Origin":ACCESS_CONTROL_ALLOW_ORIGIN
                }
            });
        }
    });
}
function comfirmOnce(body,userAuthorized,periods,bill,mlimit,countNConf){
    return new Promise((resolve, reject) => {
        let params = {
            TableName : TABLE_NUMBER1+"_"+periods,
            Key : {
                owner : userAuthorized.principalId,
                sk : "a#"+bill+"#"+body.createdate+"#"+body.number
            },
            ConditionExpression:"attribute_not_exists(#confirm) and attribute_exists(#owner) and attribute_exists(#sk) and #amountset = :amountset and #bill = :bill",
            UpdateExpression: "set #confirm = :randkey",
            ExpressionAttributeNames:{
                "#confirm":"confirm",
                "#owner":"owner",
                "#sk":"sk",
                "#bill":"bill",
                "#amountset":"amountset"
            },
            ExpressionAttributeValues:{
                ":randkey":Math.floor((Math.random()*10)%2),
                ":bill":bill,
                ":amountset":body.amountset
            },
            ReturnValues:"NONE"
        };
        
        checkBeforeComfirmOnce(body,userAuthorized,periods,mlimit,countNConf).then(function(){
            console.log(params);
            docClient.update(params, function(err, data) {
                if (err) {
                    console.log(JSON.stringify(err))
                    if(err.code === "ConditionalCheckFailedException"){
                        resolve({
                            "type":"notfound",
                            "number":body.number,
                            "createdate":body.createdate,
                            "amountset":body.amountset
                        })
                    }else if(err.code === "ResourceNotFoundException"){
                        resolve({
                            "type":"periods",
                            "number":body.number,
                            "createdate":body.createdate
                        })
                    }else{
                        let errMsg = "Unable to confirm item. Error JSON:"+ JSON.stringify(err, null, 2);
                        console.log(errMsg);
                        resolve({
                            "type":"unknow",
                            "number":body.number,
                            "createdate":body.createdate,
                            "errorcode":errMsg
                        })
                    }
                } else {
                    console.log('confirmed'+body.number)
                    resolve("confirmed");    
                }
            });
        }).catch(function(resErr){
            resolve(resErr);
        });
    });

}
function checkBeforeComfirmOnce(body,userAuthorized,periods,mlimit,countNConf){
    return new Promise(async (resolve, reject) => {
        let reallimit = 0;
        let limitall = mlimit.limitall;
        let limitbynumber = mlimit.limitbynumber;

        let countadd=0;
        if(body.number.toString() in countNConf){
            countadd = countNConf[body.number.toString()];
        }

        console.log(mlimit)
        if(body.number.toString() in limitbynumber){
            reallimit = limitbynumber[body.number.toString()];
        }else if(limitall!=='notset'){
            reallimit = limitall;
        }
        let params = {
            TableName : TABLE_NUMBER1+"_"+periods,
            Key : {
                owner : userAuthorized.toplvl,
                sk : "s#"+body.number
            }
        };
        docClient.get(params, function(err, data) {
            if (err) {
                console.log(JSON.stringify(err))
                if(err.code === "ResourceNotFoundException"){
                    reject({
                        "type":"limit",
                        "number":body.number,
                        "createdate":body.createdate,
                        "accept":reallimit-numberamount-countadd,
                        "errorcode":"ResourceNotFoundException"
                    });
                }else{
                    let errMsg = "Unable to cehck confirm item. Error JSON:"+ JSON.stringify(err, null, 2);
                    console.log(errMsg);
                    reject({
                        "type":"limit",
                        "number":body.number,
                        "createdate":body.createdate,
                        "accept":reallimit-numberamount-countadd,
                        "errorcode":errMsg
                    });
                }
            } else {
                let numberamount;
                if(!Object.keys(data).length){
                    numberamount = 0;
                }else{
                    numberamount = data.Item.amountset;
                }

                console.log("numberamount : "+numberamount)
                console.log("body.amountset : "+body.amountset)
                console.log("countadd : "+countadd)
                console.log("reallimit : "+reallimit)
                if( (numberamount+body.amountset+countadd)<=reallimit){
                    resolve();
                }else{
                    reject({
                        "type":"limit",
                        "number":body.number,
                        "createdate":body.createdate,
                        "accept":reallimit-numberamount-countadd
                    });
                }
            }
        });
    });
}
function getLimit(userAuthorized){
    return new Promise((resolve, reject) => {
        let params = {
            TableName : TABLE_USER_EXTEND,
            Key : {
                username : userAuthorized.toplvl,
                extend : "number1#gameconfig#global"
            }
        };
        docClient.get(params, function(err, data) {
            let ReturnVal = {
                "limitall":"notset",
                "limitbynumber":{}
            }
            if(err){
                resolve(ReturnVal);
            }else{
                if('limitall' in data.Item){
                    ReturnVal.limitall = data.Item.limitall
                }
                if('limitbynumber' in data.Item ){
                    ReturnVal.limitbynumber = data.Item.limitbynumber
                }
                resolve(ReturnVal);
            }
        });
    });
}
function delNumber(bodyAll,userAuthorized){
    return new Promise((resolve, reject) => {
        var allresult = [];
        bodyAll.delete.forEach(function(body){
            allresult.push(deleteOnce(body,userAuthorized,bodyAll.periods));
        });
        Promise.all(allresult).then(function(result) {
            var count = 0;
            for(var i = 0; i < result.length; i++){
                if(result[i] === "deleted"){
                    count++;
                }
            }
            resolve({
                statusCode: 200,
                body:JSON.stringify({"deleted":count}),
                headers:{
                    "Access-Control-Allow-Origin":ACCESS_CONTROL_ALLOW_ORIGIN
                }
            });
        }).catch(function(){
            resolve(ResponseConfirmError);
        });
    });
}
function deleteOnce(body,userAuthorized,periods){
    return new Promise((resolve, reject) => {
        let params = {
            TableName : TABLE_NUMBER1+"_"+periods,
            Key : {
                owner : userAuthorized.principalId,
                sk : "a#"+body.bill+"#"+body.createdate+"#"+body.number
            },
            ConditionExpression:"attribute_not_exists(#confirm) and #owner = :me",
            ExpressionAttributeNames:{"#owner":"owner","#confirm":"confirm"},
            ExpressionAttributeValues:{":me":userAuthorized.principalId},
            ReturnValues:"NONE"
        };
        docClient.delete(params, function(err, data) {
            if (err) {
                if(err.code === "ConditionalCheckFailedException"){
                    resolve("not"); 
                }else if(err.code === "ResourceNotFoundException"){
                    reject("ResourceNotFoundException");
                }else{
                    let errMsg = "Unable to confirm item. Error JSON:"+ JSON.stringify(err, null, 2);
                    console.log(errMsg);
                    resolve("not"); 
                }
            } else {
                resolve("deleted");    
            }
        });
    });

}
function getNextWednesday(next=false){
    var d = new Date();
    d.setDate(d.getDate()+ (3 + 7 - d.getDay()) % 7);
    if(next===true){
        d.setDate(d.getDate()+ 1);
        d.setDate(d.getDate()+ (3 + 7 - d.getDay()) % 7);
    }
    return d.getFullYear()+'-'+('0' +(d.getMonth()+1)).slice(-2)+'-'+('0' +(d.getDate()) ).slice(-2)
}
function checkTime(periods){
    let gameEndAtString = periods+"T"+"20:00:00+07:00";
    let gameEndAt = new Date(gameEndAtString);
    let thisTime = new Date();
    console.log("thisTime "+thisTime)
    console.log("gameEndAt "+gameEndAt)
    if(thisTime<gameEndAt){
        return true;
    }else{
        return false;
    }
}
function checkPeriods(userperiods){
    let periods = getNextWednesday()
    if(periods===userperiods){
        console.log('checktime')
        if(checkTime(periods)===true){
            return true;
        }else{
            return false;
        }
    }else{
        return false;
    }
}