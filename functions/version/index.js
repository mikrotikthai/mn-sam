const ACCESS_CONTROL_ALLOW_ORIGIN = process.env.ACCESS_CONTROL_ALLOW_ORIGIN;
const API_VERSION = process.env.API_VERSION;
const API_STAGE = process.env.API_STAGE;
const API_GITHASH = process.env.API_GITHASH;
const API_BUILDNUMBER = process.env.API_BUILDNUMBER;
const API_BUILDTIME = process.env.API_BUILDTIME;

exports.handle = async function(event, context) {
    try {
        return {
            statusCode: 200,
            body:JSON.stringify(
            {
                "version":API_VERSION,
                "stage":API_STAGE,
                "hash":API_GITHASH,
                "buildnumber":API_BUILDNUMBER,
                "buildtime":API_BUILDTIME,
            }),
            headers:{
                "Access-Control-Allow-Origin":ACCESS_CONTROL_ALLOW_ORIGIN
            }
        };
    }catch (error) {
        console.error(error);
        throw new Error(error);
    }
};