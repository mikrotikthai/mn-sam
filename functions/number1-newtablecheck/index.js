const AWS_REGION = process.env.AWS_REGION;
const TABLE_NUMBER1 = process.env.TABLE_NUMBER1;
const TABLE_GAMERESULT = process.env.TABLE_GAMERESULT;
const FUNC_NUMBER1_TRIGGER = process.env.FUNC_NUMBER1_TRIGGER;

const AWS = require("aws-sdk");
const lambda = new AWS.Lambda({region: AWS_REGION,apiVersion: '2015-03-31'});
const dynamodb = new AWS.DynamoDB({region: AWS_REGION});
const docClient = new AWS.DynamoDB.DocumentClient({region: AWS_REGION});
exports.handle = function(event, context ,callback) {
    console.log(JSON.stringify(event));
    var periods = getNextWednesday(event.time,true)
    var tableName = TABLE_NUMBER1+"_"+periods;

    var params = {
        TableName : tableName,
    };
    
    dynamodb.describeTable(params, function(err, data) {
        if(err){
            console.error(JSON.stringify(err));
            callback(JSON.stringify(err))
        }else{
            console.log(JSON.stringify(data));
            if(data.Table.TableStatus==="ACTIVE" && data.Table.StreamSpecification.StreamEnabled === true){
                addEventSourceMapping(data).then(()=>{
                    return putResult(periods,data)
                }).then(()=>{
                    callback(null,'ok');
                }).catch((err)=>{
                    callback(JSON.stringify(err));
                })
            }else{
                callback("table "+tableName+" is not active or steam is not enabled");
            }
            
        }
    });

};
function getNextWednesday(strdate,next=false){
    var di = Date.parse(strdate)
    var d = new Date(di);
    d.setDate(d.getDate()+ (3 + 7 - d.getDay()) % 7);
    if(next===true){
        d.setDate(d.getDate()+ 1);
        d.setDate(d.getDate()+ (3 + 7 - d.getDay()) % 7);
    }
    return d.getFullYear()+'-'+('0' +(d.getMonth()+1)).slice(-2)+'-'+('0' +(d.getDate()) ).slice(-2)
}
function putResult(periods,resultCreatetable){
    return new Promise(async (resolve, reject) => {
        let params = {
            TableName : TABLE_GAMERESULT,
            Item:{
                "year": Number(periods.substring(0, 4)),
                "gameperiods": "number1#"+periods,
                "status":resultCreatetable.Table.TableStatus,
                "lastupdate":Date.now(),
                "periods":periods
            }
        };
        docClient.put(params, function(err, data) {
            if(err){
                console.error(JSON.stringify(err));
                reject(JSON.stringify(err));
            }else{
                resolve();
            }
            
        });
    });
}
function addEventSourceMapping(resultCreatetable){
    return new Promise((resolve, reject) => {
        var params = {
            EventSourceArn: resultCreatetable.Table.LatestStreamArn, /* required */
            FunctionName: FUNC_NUMBER1_TRIGGER, /* required */
            BatchSize: 100,
            Enabled: true,
            StartingPosition:'TRIM_HORIZON'
          };
        lambda.createEventSourceMapping(params, function(err, data) {
            if(err){
                console.error(JSON.stringify(err));
                reject(JSON.stringify(err));
            }else{
                console.log(JSON.stringify(data))
                resolve()
            }
        });
    });
}