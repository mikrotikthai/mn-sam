const AWS_REGION = process.env.AWS_REGION;
const ACCESS_CONTROL_ALLOW_ORIGIN = process.env.ACCESS_CONTROL_ALLOW_ORIGIN;
const TABLE_USER_EXTEND = process.env.TABLE_USER_EXTEND; 

const AWS = require("aws-sdk");
const docClient = new AWS.DynamoDB.DocumentClient({region: AWS_REGION});

exports.handle = async function(event, context) {
    // console.log(JSON.stringify(event));
    let userAuthorized = event.requestContext.authorizer;
    try {
        if(event.httpMethod==="GET"){
            return await getBill(userAuthorized);
        }else{
            console.error("httpMethod not match");
        }
    }catch (error) {
        console.error(error);
        throw new Error(error);
    }
};
function getBill(userAuthorized){
    return new Promise((resolve, reject) => {
        let params = {
            TableName : TABLE_USER_EXTEND,
            Key : {
                username : userAuthorized.principalId,
                extend:"number1#bill"
            }
        };
        docClient.get(params, function(err, data) {
            if (err) {
                let errMsg = "Unable to getBill item. Error JSON:"+ JSON.stringify(err, null, 2);
                reject(errMsg);
            } else {
                if(data.Item===undefined){
                    resolve({
                        statusCode: 200,
                        body:JSON.stringify({"bill":0}),
                        headers:{
                            "Access-Control-Allow-Origin":ACCESS_CONTROL_ALLOW_ORIGIN
                        }
                    });
                }else{
                    resolve({
                        statusCode: 200,
                        body:JSON.stringify({"bill":data.Item.bill}),
                        headers:{
                            "Access-Control-Allow-Origin":ACCESS_CONTROL_ALLOW_ORIGIN
                        }
                    });
                }
            }
        });
    });
}