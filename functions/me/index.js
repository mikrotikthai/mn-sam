const AWS_REGION = process.env.AWS_REGION;
const ACCESS_CONTROL_ALLOW_ORIGIN = process.env.ACCESS_CONTROL_ALLOW_ORIGIN;
const TABLE_USER = process.env.TABLE_USER;

const AWS = require("aws-sdk");
const docClient = new AWS.DynamoDB.DocumentClient({region: AWS_REGION});
var ResponseOk = {
    statusCode: 200,
    body:JSON.stringify({"message":"ok"}),
    headers:{
        "Access-Control-Allow-Origin":ACCESS_CONTROL_ALLOW_ORIGIN
    }
};
var Response400 = {
    statusCode: 400,
    body:JSON.stringify({"message":"Invalid request body"}),
    headers:{
        "Access-Control-Allow-Origin":ACCESS_CONTROL_ALLOW_ORIGIN
    }
};
var ResponseDuplicate = {
    statusCode: 409,
    body:JSON.stringify({"message":"username is already exists"}),
    headers:{
        "Access-Control-Allow-Origin":ACCESS_CONTROL_ALLOW_ORIGIN
    }
};

exports.handle = async function(event, context) {
    // console.log(JSON.stringify(event));
    let bodyjson = JSON.parse(event.body);
    let userAuthorized = event.requestContext.authorizer;
    try {
        if(event.httpMethod==="GET"){
            return await getMe(userAuthorized);
        }else if(event.httpMethod==="PUT"){
            return await updateMe(bodyjson,userAuthorized);
        }else if(event.httpMethod==="PATCH"){
            return await changePass(bodyjson,userAuthorized);
        }else{
            console.error("httpMethod not match");
        }
    }catch (error) {
        console.error(error);
        throw new Error(error);
    }
};

function getMe(userAuthorized){
    return new Promise((resolve, reject) => {
        let params = {
            TableName : TABLE_USER,
            Key : {
                username : userAuthorized.principalId
            }
        };
        docClient.get(params, function(err, data) {
            if (err) {
                let errMsg = "Unable to read item. Error JSON:"+ JSON.stringify(err, null, 2);
                reject(errMsg);
            } else {
                if(data.Item === undefined){
                    resolve(ResponseNotfound);
                }else{
                    delete data.Item.password;
                    resolve({
                        statusCode: 200,
                        body:JSON.stringify(data.Item),
                        headers:{
                            "Access-Control-Allow-Origin":ACCESS_CONTROL_ALLOW_ORIGIN
                        }
                    });
                } 
            }
        });
    });
}
function updateMe(body,userAuthorized){
    return new Promise((resolve, reject) => {
        let UpdateStr = "set";
        let AtbValues = {};
        let AtbNames = {}; 
        if(body.name  !== undefined){
            AtbNames["#name"] = "name";
            AtbValues[":n"] = body.name;
            if(UpdateStr==="set"){
                UpdateStr+=" ";
            }else{
                UpdateStr+=" ,";
            }
            UpdateStr+=" #name = :n";
        }
        if(body.phonenumber  !== undefined){
            AtbValues[":p"] = body.phonenumber;
            if(UpdateStr==="set"){
                UpdateStr+=" ";
            }else{
                UpdateStr+=" ,";
            }
            UpdateStr+=" phonenumber = :p";
        }
        if(body.line  !== undefined){
            AtbValues[":l"] = body.line;
            if(UpdateStr==="set"){
                UpdateStr+=" ";
            }else{
                UpdateStr+=" ,";
            }
            UpdateStr+=" line = :l";
        }
        if(body.bankname  !== undefined){
            AtbValues[":bn"] = body.bankname;
            if(UpdateStr==="set"){
                UpdateStr+=" ";
            }else{
                UpdateStr+=" ,";
            }
            UpdateStr+=" bankname = :bn";
        }
        if(body.bankaccount  !== undefined){
            AtbValues[":ba"] = body.bankaccount;
            if(UpdateStr==="set"){
                UpdateStr+=" ";
            }else{
                UpdateStr+=" ,";
            }
            UpdateStr+=" bankaccount = :ba"
        }
        AtbValues[":user"] = body.username;
        AtbValues[":pass"] = body.password;
        let params = {
            TableName : TABLE_USER,
            Key : {
                username : userAuthorized.principalId
            },
            UpdateExpression: UpdateStr,
            ExpressionAttributeValues:AtbValues,
            ConditionExpression:"attribute_exists(username) and username = :user and password = :pass",
            ReturnValues:"NONE"
        };
        if(Object.keys(AtbNames).length){
            params.ExpressionAttributeNames = AtbNames;
        }
        docClient.update(params, function(err, data) {
            if (err) {
                if(!(err.code === undefined) && err.code === "ConditionalCheckFailedException"){
                    resolve(Response400);
                }else{
                    let errMsg = "Unable to read item. Error JSON:"+ JSON.stringify(err, null, 2);
                    reject(errMsg);
                }
            } else {
                resolve(ResponseOk);    
            }
        });
    });
}

function changePass(body,userAuthorized){
    return new Promise((resolve, reject) => {
        let UpdateStr = "set password = :newpass";
        let AtbValues = {};
        AtbValues[":user"] = body.username;
        AtbValues[":pass"] = body.password;
        AtbValues[":newpass"] = body.newpassword;
        let params = {
            TableName : TABLE_USER,
            Key : {
                username : userAuthorized.principalId
            },
            UpdateExpression: UpdateStr,
            ExpressionAttributeValues:AtbValues,
            ConditionExpression:"attribute_exists(username) and username = :user and password = :pass",
            ReturnValues:"NONE"
        };
        docClient.update(params, function(err, data) {
            if (err) {
                if(!(err.code === undefined) && err.code === "ConditionalCheckFailedException"){
                    resolve(Response400);
                }else{
                    let errMsg = "Unable to read item. Error JSON:"+ JSON.stringify(err, null, 2);
                    reject(errMsg);
                }
            } else {
                resolve(ResponseOk);    
            }
        });
    });
}