const AWS_REGION = process.env.AWS_REGION;
const ACCESS_CONTROL_ALLOW_ORIGIN = process.env.ACCESS_CONTROL_ALLOW_ORIGIN;
const TABLE_NUMBER1 = process.env.TABLE_NUMBER1;
const TABLE_USER = process.env.TABLE_USER;

const AWS = require("aws-sdk");
const docClient = new AWS.DynamoDB.DocumentClient({region: AWS_REGION});

var ResponseBadRequest = {
    statusCode: 400,
    body:JSON.stringify({"message":"Bad Request"}),
    headers:{
        "Access-Control-Allow-Origin":ACCESS_CONTROL_ALLOW_ORIGIN
    }
};

exports.handle = async function(event, context) {
    let queryStringParameters = event.queryStringParameters;
    let userAuthorized = event.requestContext.authorizer;
    try {
        if(event.httpMethod==="GET" && event.pathParameters===null){
            return await get(userAuthorized,queryStringParameters);
        }else if(event.httpMethod==="GET" && !(event.pathParameters===null)){
            return await get(userAuthorized,queryStringParameters,event.pathParameters.username);
        }else{
            console.error("httpMethod not match");
        }
    }catch (error) {
        console.error(error);
        throw new Error(error);
    }
};

function get(userAuthorized,queryStringParameters,username=null){
    return new Promise(async (resolve, reject) => {
        let user;
        if(username===null){
            user= userAuthorized.principalId;
        }else{
            user=username;
            let chk = await checkDownline(username,userAuthorized.principalId);
            if(chk===false){
                resolve(ResponseBadRequest);
            }
        }
        let TableName = TABLE_NUMBER1+'_'+queryStringParameters.periods
        let params = {
            RequestItems: {}
        };
        params.RequestItems[TableName] = {Keys: []};
        params.RequestItems[TableName].Keys.push({
            owner:user,
            sk:"jackpots"
        });
        params.RequestItems[TableName].Keys.push({
            owner:user,
            sk:"sumadd"
        });
        docClient.batchGet(params, function(err, data) {
            if (err){
                let errMsg = "Unable to get jackpots items. Error JSON:"+ JSON.stringify(err, null, 2);
                console.log(errMsg);
                reject(errMsg);
            }else {
                let result = {"bought":0,"boughtamountset":0,"summary":0};
                console.log(JSON.stringify(data));
                for(var i =0;i<data.Responses[TableName].length;i++){
                    if(data.Responses[TableName][i].sk==='sumadd'){
                        result.bought = data.Responses[TableName][i].price
                        result.boughtamountset = data.Responses[TableName][i].amountset
                        if('prize' in result && 'bought' in result){
                            result.summary = result.prize - result.bought
                        }
                    }else if(data.Responses[TableName][i].sk==='jackpots'){
                        Object.assign(result,data.Responses[TableName][i])
                        result.jackpotsamountset = result.amountset 
                        delete result.owner
                        delete result.sk
                        delete result.amountset
                        delete result.topowner
                        if('prize' in result && 'bought' in result){
                            result.summary = result.prize - result.bought
                        }
                    } 
                }
                resolve({
                    statusCode: 200,
                    headers:{
                        "Access-Control-Allow-Origin":ACCESS_CONTROL_ALLOW_ORIGIN
                    },
                    body:JSON.stringify(result)
                });
            }
        });
    });
}

function checkDownline(down,top){
    return new Promise((resolve, reject) => {
        let params = {
            TableName : TABLE_USER,
            Key : {
                username : down
            }
        };
        docClient.get(params, function(err, data) {
            if (err) {
                let errMsg = "Unable to read item. Error JSON:"+ JSON.stringify(err, null, 2);
                reject(errMsg);
            } else {
                if(data.Item === undefined){
                    resolve(false);
                }else{
                    if(data.Item.toplvl===top){
                        resolve(true);
                    }else{
                        resolve(false);
                    }
                }
                
                   
            }
        });
    });
}