const AWS_REGION = process.env.AWS_REGION;
const ACCESS_CONTROL_ALLOW_ORIGIN = process.env.ACCESS_CONTROL_ALLOW_ORIGIN;
const TABLE_USER = process.env.TABLE_USER;
const TABLE_USER_TOPLVL = process.env.TABLE_USER_TOPLVL;

const AWS = require("aws-sdk");
const docClient = new AWS.DynamoDB.DocumentClient({region: AWS_REGION});
var ResponseCreate = {
    statusCode: 201,
    body:JSON.stringify({"message":"ok"}),
    headers:{
        "Access-Control-Allow-Origin":ACCESS_CONTROL_ALLOW_ORIGIN
    }
};
var ResponseOk = {
    statusCode: 200,
    body:JSON.stringify({"message":"ok"}),
    headers:{
        "Access-Control-Allow-Origin":ACCESS_CONTROL_ALLOW_ORIGIN
    }
};
var ResponseNotfound = {
    statusCode: 404,
    body:JSON.stringify({"message":"username is not found"}),
    headers:{
        "Access-Control-Allow-Origin":ACCESS_CONTROL_ALLOW_ORIGIN
    }
};
var ResponseDuplicate = {
    statusCode: 409,
    body:JSON.stringify({"message":"username is already exists"}),
    headers:{
        "Access-Control-Allow-Origin":ACCESS_CONTROL_ALLOW_ORIGIN
    }
};

exports.handle = async function(event, context) {
    // console.log(JSON.stringify(event));
    let bodyjson = JSON.parse(event.body);
    let userAuthorized = event.requestContext.authorizer;
    try {
        if(event.httpMethod==="POST"){
            return await addMember(bodyjson,userAuthorized);
        }else if(event.httpMethod==="GET" && event.pathParameters===null){
            if(event.queryStringParameters ===null){
                return await getAllMember(userAuthorized);
            }else{
                let startkey,limit;
                if(!(event.queryStringParameters.startkey===undefined)){
                    let startkeyjson = new Buffer(event.queryStringParameters.startkey, 'base64').toString('ascii');
                    startkey = JSON.parse(startkeyjson);
                }else{
                    startkey = null
                }
                if(!(event.queryStringParameters.limit===undefined)){
                    limit = event.queryStringParameters.limit;
                }else{
                    limit = 10
                }
                return await getAllMember(userAuthorized,limit,startkey);
            }
        }else if(event.httpMethod==="GET"){
            return await getMember(event.pathParameters,userAuthorized);
        }else if(event.httpMethod==="PUT"){
            return await updateMember(bodyjson,userAuthorized);
        }else if(event.httpMethod==="DELETE"){
            return await delMember(event.pathParameters,userAuthorized);
        }else{
            console.error("httpMethod not match");
        }
    }catch (error) {
        console.error(error);
        throw new Error(error);
    }
};

function addMember(body,userAuthorized){
    return new Promise((resolve, reject) => {
        let params = {
            TableName : TABLE_USER,
            ConditionExpression:"attribute_not_exists(username)",
            Item:{
                "username": body.username,
                "password": body.password,
                "role":"member",
                "status":"enable",
                "toplvl":userAuthorized.principalId,
                "toplvlrole":userAuthorized.role
            }
        };
        if(body.name  !== undefined){
            params.Item.name = body.name;
        }
        if(body.phonenumber  !== undefined){
            params.Item.phonenumber = body.phonenumber;
        }
        if(body.line  !== undefined){
            params.Item.line = body.line;
        }
        if(body.bankname  !== undefined){
            params.Item.bankname = body.bankname;
        }
        if(body.bankaccount  !== undefined){
            params.Item.bankaccount = body.bankaccount;
        }
        docClient.put(params, function(err, data) {
            if (err) {
                if(!(err.code === undefined) && err.code === "ConditionalCheckFailedException"){
                    resolve(ResponseDuplicate);
                }else{
                    let errMsg = "Unable to read item. Error JSON:"+ JSON.stringify(err, null, 2);
                    reject(errMsg);
                }
            } else {
                resolve(ResponseCreate);
            }
        });
    });
}
function getAllMember(userAuthorized,limit=10,ExclusiveStartKey=null){
    return new Promise((resolve, reject) => {
        let params = {
            IndexName : TABLE_USER_TOPLVL,
            TableName : TABLE_USER,
            Limit : Number(limit),
            KeyConditionExpression : "#toplvl = :toplvl and #role = :role",
            ExpressionAttributeValues : {
                ":toplvl": userAuthorized.principalId,
                ":role": "member"
            },
            ExpressionAttributeNames : {
                "#toplvl": "toplvl",
                "#name": "name",
                "#role": "role",
                "#status": "status"
            },
            ProjectionExpression: "username,bankaccount,bankname,line,#name,password,phonenumber,#role,#status"
        };
        //console.log(params)
        if(!(ExclusiveStartKey===null)){
            params.ExclusiveStartKey = ExclusiveStartKey;
        }
        docClient.query(params, function(err, data) {
            if (err) {
                let errMsg = "Unable to read item. Error JSON:"+ JSON.stringify(err, null, 2);
                reject(errMsg);
            } else {
                //console.log(data)
                delete data.ScannedCount;
                if(!(data.LastEvaluatedKey===undefined)){
                    let Lk = data.LastEvaluatedKey;
                    let Lkjson = JSON.stringify(Lk);
                    let newLk = new Buffer(Lkjson).toString('base64');
                    data.LastEvaluatedKey = newLk;
                }
                resolve({
                    statusCode: 200,
                    headers:{
                        "Access-Control-Allow-Origin":ACCESS_CONTROL_ALLOW_ORIGIN
                    },
                    body:JSON.stringify(data)
                });    
            }
        });
    });
}
function getMember(pathParameters,userAuthorized){
    return new Promise((resolve, reject) => {
        let params = {
            TableName : TABLE_USER,
            Key : {
                username : pathParameters.username
            }
        };
        docClient.get(params, function(err, data) {
            if (err) {
                if(!(err.code === undefined) && err.code === "ConditionalCheckFailedException"){
                    resolve(ResponseNotfound);
                }else{
                    let errMsg = "Unable to read item. Error JSON:"+ JSON.stringify(err, null, 2);
                    reject(errMsg);
                }
            } else {
                if(data.Item === undefined){
                    resolve(ResponseNotfound);
                }else{
                    if(data.Item.role==='member' && data.Item.toplvl === userAuthorized.principalId){
                        resolve({
                            statusCode: 200,
                            body:JSON.stringify(data.Item),
                            headers:{
                                "Access-Control-Allow-Origin":ACCESS_CONTROL_ALLOW_ORIGIN
                            }
                        });
                    }else{
                        resolve(ResponseNotfound);
                    }
                }
                
                   
            }
        });
    });
}
function updateMember(body,userAuthorized){
    return new Promise((resolve, reject) => {
        let UpdateStr = "set";
        let rm = "remove";
        let AtbValues = {};
        let AtbNames = {}; 
        if(body.password  !== undefined){
            AtbValues[":pass"] = body.password;
            if(UpdateStr==="set"){
                UpdateStr+=" ";
            }else{
                UpdateStr+=" ,";
            }
            UpdateStr+=" password = :pass";
        }
        if(body.name  !== undefined){
            AtbNames["#name"] = "name";
            AtbValues[":n"] = body.name;
            if(UpdateStr==="set"){
                UpdateStr+=" ";
            }else{
                UpdateStr+=" ,";
            }
            UpdateStr+=" #name = :n";
        }
        if(body.phonenumber  !== undefined){
            AtbValues[":p"] = body.phonenumber;
            if(UpdateStr==="set"){
                UpdateStr+=" ";
            }else{
                UpdateStr+=" ,";
            }
            UpdateStr+=" phonenumber = :p";
        }else{
            if(rm==="remove"){
                rm+=" ";
            }else{
                rm+=" ,";
            }
            rm+= " phonenumber";
        }
        if(body.line  !== undefined){
            AtbValues[":l"] = body.line;
            if(UpdateStr==="set"){
                UpdateStr+=" ";
            }else{
                UpdateStr+=" ,";
            }
            UpdateStr+=" line = :l";
        }else{
            if(rm==="remove"){
                rm+=" ";
            }else{
                rm+=" ,";
            }
            rm+= " line";
        }
        if(body.bankname  !== undefined){
            AtbValues[":bn"] = body.bankname;
            if(UpdateStr==="set"){
                UpdateStr+=" ";
            }else{
                UpdateStr+=" ,";
            }
            UpdateStr+=" bankname = :bn";
        }else{
            if(rm==="remove"){
                rm+=" ";
            }else{
                rm+=" ,";
            }
            rm+= " bankname";
        }
        if(body.bankaccount  !== undefined){
            AtbValues[":ba"] = body.bankaccount;
            if(UpdateStr==="set"){
                UpdateStr+=" ";
            }else{
                UpdateStr+=" ,";
            }
            UpdateStr+=" bankaccount = :ba"
        }else{
            if(rm==="remove"){
                rm+=" ";
            }else{
                rm+=" ,";
            }
            rm+= " bankaccount";
        }
        if(body.status  !== undefined){
            AtbValues[":s"] = body.status;
            if(UpdateStr==="set"){
                UpdateStr+=" ";
            }else{
                UpdateStr+=" ,";
            }
            UpdateStr+=" status = :s"
        }
        AtbValues[":me"] = userAuthorized.principalId;
        if(rm!=="remove"){
            UpdateStr+= " "+rm
        }
        let params = {
            TableName : TABLE_USER,
            Key : {
                username : body.username
            },
            UpdateExpression: UpdateStr,
            ExpressionAttributeValues:AtbValues,
            ConditionExpression:"attribute_exists(username) and toplvl = :me",
            ReturnValues:"NONE"
        };
        if(Object.keys(AtbNames).length){
            params.ExpressionAttributeNames = AtbNames;
        }
        docClient.update(params, function(err, data) {
            if (err) {
                if(!(err.code === undefined) && err.code === "ConditionalCheckFailedException"){
                    resolve(ResponseNotfound);
                }else{
                    let errMsg = "Unable to update item. Error JSON:"+ JSON.stringify(err, null, 2);
                    reject(errMsg);
                }
            } else {
                resolve(ResponseOk);    
            }
        });
    });
}
function delMember(pathParameters,userAuthorized){
    return new Promise((resolve, reject) => {
        let AtbValues =  {};
        AtbValues[":me"] = userAuthorized.principalId;
        let params = {
            TableName : TABLE_USER,
            ExpressionAttributeValues:AtbValues,
            ConditionExpression:"attribute_exists(username) and toplvl = :me",
            Key : {
                username : pathParameters.username
            }
        };
        docClient.delete(params, function(err, data) {
            if (err) {
                if(!(err.code === undefined) && err.code === "ConditionalCheckFailedException"){
                    resolve(ResponseNotfound);
                }else{
                    let errMsg = "Unable to read item. Error JSON:"+ JSON.stringify(err, null, 2);
                    reject(errMsg);
                }
            } else {
                resolve(ResponseOk);
            }
        });
    });
}