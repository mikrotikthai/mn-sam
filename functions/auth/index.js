const fs = require('fs');
const CERT_PUBLIC_KEY = fs.readFileSync('public.pem');
const jwt = require('jsonwebtoken');
const PERMISSION = {
    "/POST/member":["mb:ad","mb:c"],
    "/GET/member":["mb:ad","mb:r"],
    "/PUT/member":["mb:ad","mb:u"],
    "/DELETE/member":["mb:ad","mb:d"],
    "/POST/number1":["n1:ad","n1:c"],
    "/DELETE/number1":["n1:ad","n1:d"],
    "/PATCH/number1":["n1:ad","n1:u"],
    "/GET/number1":["n1:ad","n1:r"],
    "/GET/number1/bill":["n1b:r"],
    "/PUT/number1/config":["n1c:ad","n1c:u"],
    "/PATCH/number1/config":["n1c:ad","n1c:u"],
    "/GET/number1/config":["n1c:ad","n1c:r"],
    "/DELETE/number1/config":["n1c:ad","n1c:d"],
    "/PATCH/number1/result":["n1r:ad","n1r:u"],
    "/GET/number1/result":["n1r:ad","n1r:r"],
    "/GET/number1/credit":["n1cd:ad","n1cd:r"],
    "/PUT/number1/credit":["n1cd:ad","n1cd:u"],
    "/DELETE/number1/credit":["n1cd:ad","n1cd:d"],
    "/GET/number1/jackpots":["n1jp:r"],
    "/GET/number1/jackpotslist":["n1jpl:r"],
    "/GET/number1/payjackpots":["n1pjp:r"],
    "/GET/number1/payjackpotslist":["n1pjpl:r"],
    "/POST/number1/send":["n1s:c"],
    "/GET/me":["me:ad"],
    "/PUT/me":["me:ad"],
    "/PATCH/me":["me:ad"],
    "/GET/2fa":["me:ad"],
    "/POST/2fa":["me:ad"],
    "/DELETE/2fa":["me:ad"]
}
exports.handle = function(event, context,callback) {
    console.log(JSON.stringify(event));
    verifyToken(event.authorizationToken,event.methodArn).then(function(authResult){
        switch (authResult.action) {
            case 'allow':
                callback(null,generatePolicy(authResult.payload, 'Allow', event.methodArn));
                break;
            case 'deny':
                callback(null,generatePolicy(authResult.payload, 'Deny', event.methodArn));
                break;
            case 'unauthorized':
                callback("Unauthorized");   // Return a 401 Unauthorized response
                break;
            default:
                callback("Error: Invalid token"); 
        }
    });

};

var verifyToken = function(token,resource) {
    return new Promise((resolve, reject) => {
        jwt.verify(token, CERT_PUBLIC_KEY, { algorithms: ['RS256'] }, function (err, payload) {
            // if token alg != RS256,  err == invalid signature
            if(err){
                resolve({action:'unauthorized'});
            }else{
                //var rexResult = resource.match("\/(POST|GET|PUT|DELETE|PATCH)\/[a-z0-9]{1,}");
                var rexResult = resource.match("\/(POST|GET|PUT|DELETE|PATCH)\/(number1\/([a-z0-9]{1,})|[a-z0-9]{1,})");
                console.log(rexResult);
                var permissionNeed = PERMISSION[rexResult[0]];
                checkPermission(permissionNeed,payload.permission).then(function(){
                    resolve({action:'allow',payload:payload});
                }).catch(function(){
                    resolve({action:'deny',payload:payload});
                })
            }
        });
    });
}
var checkPermission = function(permissionNeed,permission){
    return new Promise((resolve, reject) => {
        for(var i=0;i<permissionNeed.length;i++){
            if(!(permission.indexOf(permissionNeed[i]) == -1)){
                resolve();
            }
        }
        reject();
    });
}
var generatePolicy = function(payload, effect, resource) {
    var authResponse = {};
    
    authResponse.principalId = payload.username;
    if (effect && resource) {
        var policyDocument = {};
        policyDocument.Version = '2012-10-17'; 
        policyDocument.Statement = [];
        var statementOne = {};
        statementOne.Action = 'execute-api:Invoke'; 
        statementOne.Effect = effect;
        statementOne.Resource = resource;
        policyDocument.Statement[0] = statementOne;
        authResponse.policyDocument = policyDocument;
    }
    
    // Optional output with custom properties of the String, Number or Boolean type.
    authResponse.context = {
        "role": payload.role,
        "permission": payload.permission,
        "toplvl":payload.toplvl,
        "toplvlrole":payload.toplvlrole,
    };
    return authResponse;
}