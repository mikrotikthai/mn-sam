const AWS_REGION = process.env.AWS_REGION;
const INDEX_NUMBER1_CONFIRM = process.env.INDEX_NUMBER1_CONFIRM;
const TABLE_NUMBER1 = process.env.TABLE_NUMBER1;
const INDEX_NUMBER1_DOWN = process.env.INDEX_NUMBER1_DOWN;
const INDEX_NUMBER1_TOP = process.env.INDEX_NUMBER1_TOP;
const TABLE_USER = process.env.TABLE_USER;
const TABLE_USER_EXTEND = process.env.TABLE_USER_EXTEND; 

const AWS = require("aws-sdk");
const docClient = new AWS.DynamoDB.DocumentClient({region: AWS_REGION});

var ResponseBadRequest = {
    statusCode: 400,
    body:JSON.stringify({"message":"Bad Request"})
};
var resultset,periods,confirmkey;
exports.handle = function(event, context,callback) {
    console.log(JSON.stringify(event));
    let payload = JSON.parse(event.Records[0].Sns.Message);
    resultset = payload.resultset;
    periods = payload.periods;
    confirmkey = payload.confirmkey;
    console.log('start..........');
    calJackpots().then(function(){
        console.log('end..........');
        callback(null,'ok')
    }).catch(function(err){
        callback(err)
    });     
};

function calJackpots(startkey=null){
    return new Promise((resolve, reject) => {
        var params = {
            IndexName : INDEX_NUMBER1_CONFIRM,
            TableName : TABLE_NUMBER1+"_"+periods,
            KeyConditionExpression : "#confirm = :confirm",
            ExpressionAttributeNames :{"#confirm":"confirm"},
            ExpressionAttributeValues : {":confirm":confirmkey},
            Limit: 5
        };
        if(startkey!==null){
            params.ExclusiveStartKey = startkey
        }
        docClient.query(params, function(err, data) {
            if (err){
                console.log(err);
                reject(err)
            }else{
                let waitall = [];
                console.log(data);
                for(var i=0;i<data.Items.length;i++){
                    console.log(i);
                    waitall.push(calJackpotsPartSegment(data.Items[i]));
                }
                Promise.all(waitall).then(function(){
                    if('LastEvaluatedKey' in data){
                        calJackpots(data.LastEvaluatedKey).then(function(){
                            resolve();
                        }).catch(function(err){
                            reject(err)
                        })
                    }else{
                        resolve();
                    }
                });
            }
            
        });

    });
}
function calJackpotsPartSegment(item){
    return new Promise((resolve, reject) => {
        let jackpots = 'missmatch';
        if('4match' in item.prizepool){
            if(item.number === resultset['4match']){
                jackpots = '4match';
            }
        }
        if(jackpots=== 'missmatch' && '4combi' in item.prizepool){
            for(var i=0;i<resultset['4combi'].length;i++){
                if(item.number === resultset['4combi'][i]){
                    jackpots = '4combi';
                    break;
                }
            }
        }
        if(jackpots=== 'missmatch' && '3match' in item.prizepool){
            if(item.number.substring(1, 4) === resultset['3match']){
                jackpots = '3match';
            }
        }
        if(jackpots=== 'missmatch' && '3combi' in item.prizepool){
            for(var i=0;i<resultset['3combi'].length;i++){
                if(item.number.substring(1, 4) === resultset['3combi'][i]){
                    jackpots = '3combi';
                    break;
                }
            }
        }
        if(jackpots=== 'missmatch' && '2frist' in item.prizepool){
            if(item.number.substring(0, 2) === resultset['2frist']){
                jackpots = '2frist';
            }
        }
        if(jackpots=== 'missmatch' && '2last' in item.prizepool){
            if(item.number.substring(2, 4) === resultset['2last']){
                jackpots = '2last';
            }
        }

        
        if(jackpots==='missmatch'){
           
            if('jackpots' in item){
                removeJackpots(item).then(function(){
                    resolve()
                }).catch(function(err){
                    reject(err)
                })
            }else{
                resolve()
            }
        }else{
            // update jackpots
            console.log(jackpots+" "+item.number)
            updateJackpots(item,jackpots).then(function(){
                resolve()
            }).catch(function(err){
                reject(err)
            })
        }
    });
}

function updateJackpots(item,jackpots){
    return new Promise((resolve, reject) => {
        let params = {
            TableName : TABLE_NUMBER1+"_"+periods,
            Key : {
                owner : item.owner,
                sk : item.sk
            },
            UpdateExpression: "set #jackpots = :jackpots",
            ExpressionAttributeNames:{"#jackpots":"jackpots"},
            ExpressionAttributeValues:{":jackpots":jackpots},
            ReturnValues:"NONE"
        };
        docClient.update(params, function(err, data) {
            if(err){
                reject(JSON.stringify(err));
            }else{
                resolve();
            }
        })
    });
}

function removeJackpots(item){
    return new Promise((resolve, reject) => {
        let params = {
            TableName : TABLE_NUMBER1+"_"+periods,
            Key : {
                owner : item.owner,
                sk : item.sk
            },
            UpdateExpression: "REMOVE #jackpots",
            ExpressionAttributeNames:{"#jackpots":"jackpots"},
            ReturnValues:"NONE"
        };
        docClient.update(params, function(err, data) {
            if(err){
                reject(JSON.stringify(err));
            }else{
                resolve();
            }
        })
    });
}