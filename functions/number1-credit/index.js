const AWS_REGION = process.env.AWS_REGION;
const ACCESS_CONTROL_ALLOW_ORIGIN = process.env.ACCESS_CONTROL_ALLOW_ORIGIN;
const TABLE_USER_EXTEND = process.env.TABLE_USER_EXTEND; 
const TABLE_NUMBER1 = process.env.TABLE_NUMBER1;
const TABLE_USER = process.env.TABLE_USER;

const AWS = require("aws-sdk");
const docClient = new AWS.DynamoDB.DocumentClient({region: AWS_REGION});
var ResponseOk = {
    statusCode: 200,
    body:JSON.stringify({"message":"ok"}),
    headers:{
        "Access-Control-Allow-Origin":ACCESS_CONTROL_ALLOW_ORIGIN
    }
};
var Response400 = {
    statusCode: 400,
    body:JSON.stringify({"message":"Invalid request"}),
    headers:{
        "Access-Control-Allow-Origin":ACCESS_CONTROL_ALLOW_ORIGIN
    }
};
var ResponseNotfound = {
    statusCode: 404,
    body:JSON.stringify({"message":"config not found"}),
    headers:{
        "Access-Control-Allow-Origin":ACCESS_CONTROL_ALLOW_ORIGIN
    }
};

exports.handle = async function(event, context) {
    console.log(JSON.stringify(event));
    let bodyjson = JSON.parse(event.body);
    let userAuthorized = event.requestContext.authorizer;
    try {
        if(event.httpMethod==="GET"){
            return await getCredit(userAuthorized,event.queryStringParameters);
        }else if(event.httpMethod==="PUT"){
            return await updateCredit(bodyjson,userAuthorized);
        }else{
            console.error("httpMethod not match");
        }
    }catch (error) {
        console.error(error);
        throw new Error(error);
    }
};
function getNextWednesday(){
    var d = new Date();
    d.setDate(d.getDate() + (3 + 7 - d.getDay()) % 7);
    return d.getFullYear()+'-'+('0' +(d.getMonth()+1)).slice(-2)+'-'+('0' +d.getDate()).slice(-2)
}
function getCredit(userAuthorized,queryStringParameters){
    return new Promise(async(resolve, reject) => {
        let user;
        if(queryStringParameters !==null && ('username' in queryStringParameters)){
            if( !(userAuthorized.role === "member")){
                let downline = await checkDownline(queryStringParameters.username,userAuthorized.principalId);
                if(downline===true){
                    user = queryStringParameters.username;
                }else{
                    resolve(Response400);
                }
            }else{
                resolve(Response400);
            }
        }else{
            if((userAuthorized.role === "member")){ // only member have cradit
                user = userAuthorized.principalId;
            }else{
                resolve(Response400);
            }
        }
        let TableNum = TABLE_NUMBER1+"_"+getNextWednesday();
        let params = {
            RequestItems: {}
        };
        params.RequestItems[TABLE_USER_EXTEND] ={
            Keys: [
                {
                    username:user,
                    extend:"number1#credit"
                }
            ]
        }
        params.RequestItems[TableNum]= {
            Keys: [
                {
                    owner:user,
                    sk:"sumadd"
                },
            ]
        }
        console.log(params)
        docClient.batchGet(params, function(err, data) {
            let result = {};
            if (err){
                let errMsg = "Unable to getconfig items. Error JSON:"+ JSON.stringify(err, null, 2);
                console.log(errMsg);
                reject(errMsg);
            }else {
                console.log(JSON.stringify(data));
                if(data.Responses[TABLE_USER_EXTEND].length === 0){
                    result.credit = 0;
                }else{
                    result.credit = data.Responses[TABLE_USER_EXTEND][0].credit;  
                }
                if(data.Responses[TableNum].length === 0){
                    result.usedcredit = 0;
                }else{
                    result.usedcredit = data.Responses[TableNum][0].price;
                }
                resolve({
                    statusCode: 200,
                    body:JSON.stringify(result),
                    headers:{
                        "Access-Control-Allow-Origin":ACCESS_CONTROL_ALLOW_ORIGIN
                    }
                });
            }
        });
    });
}
function updateCredit(bodyjson,userAuthorized){
    return new Promise(async(resolve, reject) => {
        let downline = await checkDownline(bodyjson.username,userAuthorized.principalId);
        if(!(downline===true)){
            resolve(Response400);
        }else{
            let params = {
                TableName : TABLE_USER_EXTEND,
                Item : {
                    username : bodyjson.username,
                    extend:"number1#credit",
                    credit:bodyjson.credit
                }
            };
            docClient.put(params, function(err, data) {
                if (err) {
                    let errMsg = "Unable to read item. Error JSON:"+ JSON.stringify(err, null, 2);
                    reject(errMsg);
                } else {
                    resolve(ResponseOk);
                }
            });
        }
    });
}
function checkDownline(down,top){
    return new Promise((resolve, reject) => {
        let params = {
            TableName : TABLE_USER,
            Key : {
                username : down
            }
        };
        docClient.get(params, function(err, data) {
            if (err) {
                let errMsg = "Unable to read item. Error JSON:"+ JSON.stringify(err, null, 2);
                reject(errMsg);
            } else {
                if(data.Item === undefined){
                    resolve(false);
                }else{
                    if(data.Item.toplvl===top){
                        resolve(true);
                    }else{
                        resolve(false);
                    }
                }
                
                   
            }
        });
    });
}