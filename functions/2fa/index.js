const AWS_REGION = process.env.AWS_REGION;
const ACCESS_CONTROL_ALLOW_ORIGIN = process.env.ACCESS_CONTROL_ALLOW_ORIGIN;
const TABLE_USER = process.env.TABLE_USER;
const SECRET_LENGTH = 64;
const AWS = require("aws-sdk");
const docClient = new AWS.DynamoDB.DocumentClient({region: AWS_REGION});
const speakeasy = require('speakeasy');
const QRCode = require('qrcode');
var ResponseOk = {
    statusCode: 200,
    body:JSON.stringify({"message":"ok"}),
    headers:{
        "Access-Control-Allow-Origin":ACCESS_CONTROL_ALLOW_ORIGIN
    }
};
var Response400 = {
    statusCode: 400,
    body:JSON.stringify({"message":"Invalid request body"}),
    headers:{
        "Access-Control-Allow-Origin":ACCESS_CONTROL_ALLOW_ORIGIN
    }
};


exports.handle = async function(event, context) {
    // console.log(JSON.stringify(event));
    let bodyjson = JSON.parse(event.body);
    let userAuthorized = event.requestContext.authorizer;
    try {
        if(event.httpMethod==="GET"){
            return await getNew2fa(userAuthorized);
        }else if(event.httpMethod==="POST"){
            return await create2fa(bodyjson,userAuthorized);
        }else if(event.httpMethod==="DELETE"){
            return await delete2fa(bodyjson,userAuthorized);
        }else{
            console.error("httpMethod not match");
        }
    }catch (error) {
        console.error(error);
        throw new Error(error);
    }
};
function getNew2fa(userAuthorized){
    return new Promise(async (resolve, reject) => {
        let result ={};
        let options = {
            issuer: `Magic Number`,
            name: 'MN - '+userAuthorized.principalId,
            length: SECRET_LENGTH
        };
        let c2fa = await chk2fa(userAuthorized);
        if(c2fa===false){
            let secret = speakeasy.generateSecret(options);
            result.secret = secret.base32;
            QRCode.toDataURL(secret.otpauth_url, function(err, image_data) {
                result.qr = image_data;
                resolve({
                    statusCode: 200,
                    body:JSON.stringify(result),
                    headers:{
                        "Access-Control-Allow-Origin":ACCESS_CONTROL_ALLOW_ORIGIN
                    }
                })
            });
        }else{
            resolve(Response400)
        }

    });
}
function chk2fa(userAuthorized){
    return new Promise(async (resolve, reject) => {
        let params = {
            TableName : TABLE_USER,
            Key : {
                username : userAuthorized.principalId
            },
            ProjectionExpression: "#2fa",
            ExpressionAttributeNames:{"#2fa":"2fa"}
        };
        docClient.get(params, function(err, data) {
            console.log(data);
            if (err) {
                let errMsg = "Unable to read item. Error JSON:"+ JSON.stringify(err, null, 2);
                console.log(errMsg);
                resolve(false);
            } else {
                if(!Object.keys(data.Item).length){
                    resolve(false);
                }else{
                    if('2fa' in data.Item){
                        resolve(true);    
                    }else{
                        resolve(false);
                    }
                }
            }
        });
    });
}
function create2fa(bodyjson,userAuthorized){
    return new Promise(async (resolve, reject) => {
        let verified = speakeasy.totp.verify({
            secret: bodyjson.secret,
            encoding: 'base32',
            token: bodyjson.userToken
        });
        if(verified===true){
            let params = {
                TableName : TABLE_USER,
                Key : {
                    username : userAuthorized.principalId
                },
                UpdateExpression: "SET #2fa = :secret",
                ExpressionAttributeNames:{"#2fa":"2fa"},
                ExpressionAttributeValues:{":secret":bodyjson.secret},
                ConditionExpression:"attribute_not_exists(#2fa)",
                ReturnValues:"NONE"
            };
            docClient.update(params, function(err, data) {
                if (err) {
                    if(!(err.code === undefined) && err.code === "ConditionalCheckFailedException"){
                        resolve(Response400);
                    }else{
                        let errMsg = "Unable to read item. Error JSON:"+ JSON.stringify(err, null, 2);
                        reject(errMsg);
                    }
                } else {
                    resolve(ResponseOk);    
                }
            });
        }else{
            resolve(Response400)
        }
    });
}
function delete2fa(bodyjson,userAuthorized){
    return new Promise(async (resolve, reject) => {
        let secret = await getSecret(userAuthorized);
        let verified = speakeasy.totp.verify({
            secret: secret,
            encoding: 'base32',
            token: bodyjson.userToken
        });
        if(verified===true){
            let params = {
                TableName : TABLE_USER,
                Key : {
                    username : userAuthorized.principalId
                },
                UpdateExpression: "REMOVE #2fa",
                ExpressionAttributeNames:{"#2fa":"2fa"},
                ReturnValues:"NONE"
            };
            docClient.update(params, function(err, data) {
                if (err) {
                    if(!(err.code === undefined) && err.code === "ConditionalCheckFailedException"){
                        resolve(Response400);
                    }else{
                        let errMsg = "Unable to read item. Error JSON:"+ JSON.stringify(err, null, 2);
                        reject(errMsg);
                    }
                } else {
                    resolve(ResponseOk);    
                }
            });
        }else{
            resolve(Response400)
        }
    });
}
function getSecret(userAuthorized){
    return new Promise((resolve, reject) => {
        let params = {
            TableName : TABLE_USER,
            Key : {
                username : userAuthorized.principalId
            },
            ProjectionExpression:"#2fa",
            ExpressionAttributeNames:{"#2fa":"2fa"}
        };
        docClient.get(params, function(err, data) {
            if (err) {
                let errMsg = "Unable to cannot_get_secret item. Error JSON:"+ JSON.stringify(err, null, 2);
                console.log(errMsg);
                resolve('cannot_get_secret');
            } else {
                
                resolve(data.Item['2fa']);    
            }
        });
    });
}