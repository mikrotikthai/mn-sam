const AWS_REGION = process.env.AWS_REGION;
const ACCESS_CONTROL_ALLOW_ORIGIN = process.env.ACCESS_CONTROL_ALLOW_ORIGIN;
const TABLE_USER = process.env.TABLE_USER;
const TABLE_USER_EXTEND = process.env.TABLE_USER_EXTEND; 
const LOGIN_FAILD_COUNT = 5;
const LOGIN_FAILD_RATE = 10;
const LOGIN_FAILD_WAIT = 30;

const fs = require('fs');
const CERT_PRIVATE_KEY = fs.readFileSync('private.key');
const PERMISSION_MEMBER = process.env.PERMISSION_MEMBER;
const PERMISSION_AGENT = process.env.PERMISSION_AGENT;

const USERROLE = {
    "member":PERMISSION_MEMBER,
    "agent":PERMISSION_AGENT,
    "masteragent":PERMISSION_AGENT,
};

const jwt = require('jsonwebtoken');
const AWS = require("aws-sdk");
const speakeasy = require('speakeasy');
// const ssm = new AWS.SSM({region: AWS_REGION,apiVersion: '2014-11-06'});
// function getParameters(){
//     return new Promise(async (resolve, reject) => {
//         console.log('aaaaaaaaaaaaa')
//         const params = {
//             Names: ['pmn-private1'],
//             WithDecryption: true
//         };
//         ssm.getParameters(params, function(err, data) {
//             if (err){
//                 console.log(err, err.stack); // an error occurred
//                 resolve()
//             }else{
//                 //CERT_PRIVATE_KEY = data.Parameters[0].Value
//                 console.log(data);           // successful response
//                 resolve()
//             }
//         });
//     });
// }

const docClient = new AWS.DynamoDB.DocumentClient({region: AWS_REGION});
var Response = {
    statusCode: 423,
    body:JSON.stringify({"message":"You have been blocked"}),
    headers:{
        "Access-Control-Allow-Origin":ACCESS_CONTROL_ALLOW_ORIGIN,
        "X-Rate-Limit":5
    }
};

exports.handle = async function(event, context) {
    console.log(JSON.stringify(event));
    //await getParameters();
    let bodyjson = JSON.parse(event.body);
    try {
        let user,chk;
        chk = await loginchk(event.headers['X-Forwarded-For']);
        if(chk.status === 'allow'){
            user = await checkUser(bodyjson.username,bodyjson.password);
            if(user === false){
                Response.statusCode = 400;
                let result = await loginfailed(event.headers['X-Forwarded-For']);
                Response.body = JSON.stringify({
                    "message":"Invalid username/password supplied",
                    "X-Login-Faild-Count":result.count,
                    "X-Client-Ip":result.ip
                });
                
                Response.headers['X-Login-Faild-Count'] = result.count;
                Response.headers['X-Client-Ip'] = result.ip;
            }else{
                let verified;
                if('2fa' in user){
                    verified = speakeasy.totp.verify({
                        secret: user['2fa'],
                        encoding: 'base32',
                        token: bodyjson['2fa']
                    });
                }else{
                    verified = true;
                }
                if(verified===true){
                    var cert = CERT_PRIVATE_KEY;
                    var payload = {
                        username:user.username,
                        permission:USERROLE[user.role]+",me:ad",
                        toplvl:user.toplvl,
                        toplvlrole:user.toplvlrole,
                        role:user.role
                    }
                    var token = jwt.sign(payload, cert, { algorithm: 'RS256',expiresIn: '3600000'});
                    Response.statusCode = 200;
                    Response.body = JSON.stringify({
                        "token":token,
                        "expin": Date.now()+3600000,
                        "username":user.username,
                        "role":user.role,
                        "toplvl":user.toplvl,
                        "toplvlrole":user.toplvlrole,
                        "X-Login-Faild-Count":0,
                        "X-Client-Ip":event.headers['X-Forwarded-For']
                    });
                    Response.headers['X-Login-Faild-Count'] = 0;
                    Response.headers['X-Client-Ip'] = event.headers['X-Forwarded-For'];
                    await loginok(event.headers['X-Forwarded-For']);
                }else{
                    Response.statusCode = 400;
                    let result = await loginfailed(event.headers['X-Forwarded-For']);
                    Response.body = JSON.stringify({
                        "message":"Invalid 2fa",
                        "X-Login-Faild-Count":result.count,
                        "X-Client-Ip":result.ip
                    });
                    Response.headers['X-Login-Faild-Count'] = result.count;
                    Response.headers['X-Client-Ip'] = result.ip;
                }
            }
        }else{
            Response.statusCode = 423;
            Response.body = JSON.stringify({
                "message":"You have been blocked",
                "X-Client-Ip":chk.ip,
                "X-Login-Faild-Count":5,
                "X-Locked-Time":chk.locked,
                "X-Locked-Exp":chk.exp
            });
            Response.headers['X-Client-Ip'] = chk.ip;
            Response.headers['X-Login-Faild-Count'] = 5;
            Response.headers['X-Locked-Time'] = chk.locked;
            Response.headers['X-Locked-Exp'] = chk.exp;
        }
        return Response;
    }catch (error) {
        console.error(error);
        throw new Error(error);
    }
};


function checkUser(user,pass){
    var params = {
        TableName : TABLE_USER,
        Key: {
            username: user
        }
    };
    return new Promise((resolve, reject) => {
        docClient.get(params, function(err, data) {
            if (err) {
                let errMsg = "Unable to read item. Error JSON:"+ JSON.stringify(err, null, 2);
                reject(errMsg)
            } else {
                if(Object.keys(data).length){
                    if(data.Item.password === pass){
                        if(data.Item.status==="enable"){
                            resolve(data.Item);
                        }else{
                            resolve(false);
                        }
                    }else{
                        resolve(false);
                    }
                }else{
                    resolve(false);
                }
            }
        });
    });

}

function loginfailed(clientip){
    return new Promise((resolve, reject) => {
        let params = {
            TableName : TABLE_USER_EXTEND,
            Key : {
                username : clientip,
                extend : 'loginfaild'
            },
            UpdateExpression: "ADD #count :count SET #exp = :exp",
            ExpressionAttributeValues:{
                ":count":1,
                ":exp":Math.floor(Date.now()/1000)+(LOGIN_FAILD_RATE*60)
            },
            ExpressionAttributeNames:{
                "#count":"count",
                "#exp":"exp"
            },
            ReturnValues:"ALL_NEW"
        };
        docClient.update(params, function(err, data) {
            if (err) {
                let errMsg = "Unable to update item loginfailed. Error JSON:"+ JSON.stringify(err, null, 2);
                console.log(errMsg);
                resolve(99999);
            } else {
                let lockedCount = 1 ;
                if('lockedCount' in data.Attributes){
                    lockedCount = data.Attributes.lockedCount*2;
                }
                if(data.Attributes.count===LOGIN_FAILD_COUNT){
                    clientLocked(clientip,lockedCount).then(function(){
                        resolve({count:5,ip:clientip});   
                    });
                }else{
                    resolve({count:data.Attributes.count,ip:data.Attributes.username});   
                }
            }
        });
    });
}
function loginok(clientip){
    return new Promise((resolve, reject) => {
        let params = {
            TableName : TABLE_USER_EXTEND,
            Key : {
                username : clientip,
                extend : 'loginfaild'
            },
            ReturnValues:"NONE"
        };
        docClient.delete(params, function(err, data) {
            if (err) {
                let errMsg = "Unable to delete item loginok. Error JSON:"+ JSON.stringify(err, null, 2);
                console.log(errMsg);
                resolve();
            } else {
                resolve();    
            }
        });
    });
}
function clientLocked(clientip,lockedCount){
    return new Promise((resolve, reject) => {
        let params = {
            TableName : TABLE_USER_EXTEND,
            Key : {
                username : clientip,
                extend : 'loginfaild'
            },
            UpdateExpression: "SET #count = :count,#lockedCount = :lockedCount,#locked = :locked,#lockedexp = :lockedexp REMOVE #exp",
            ExpressionAttributeValues:{
                ":count":0,
                ":lockedCount":lockedCount,
                ":locked":(LOGIN_FAILD_WAIT*60*lockedCount),
                ":lockedexp":Math.floor(Date.now()/1000)+(LOGIN_FAILD_WAIT*60*lockedCount)
            },
            ExpressionAttributeNames:{
                "#count":"count",
                "#lockedCount":"lockedCount",
                "#locked":"locked",
                "#lockedexp":"lockedexp",
                "#exp":"exp"
            },
            ReturnValues:"NONE"
        };
        docClient.update(params, function(err, data) {
            if (err) {
                let errMsg = "Unable to update item clientLocked. Error JSON:"+ JSON.stringify(err, null, 2);
                console.log(errMsg);
                resolve();
            } else {
                resolve();   
            }
        });
    });
}
function loginchk(clientip){
    return new Promise((resolve, reject) => {
        let params = {
            TableName : TABLE_USER_EXTEND,
            Key : {
                username : clientip,
                extend : 'loginfaild'
            }
        };
        docClient.get(params, function(err, data) {
            if (err) {
                let errMsg = "Unable to get item loginchk. Error JSON:"+ JSON.stringify(err, null, 2);
                console.log(errMsg);
                resolve();
            } else {
                let result={};
                console.log(data);
                if(Object.keys(data).length === 0){
                    result.status = 'allow';
                    result.count = 0
                    result.ip = clientip
                }else if('locked' in data.Item){
                    if( data.Item.lockedexp>Math.floor(Date.now()/1000) ){
                        result.status = 'deny';
                        result.ip = clientip
                        result.locked = data.Item.locked
                        result.exp = data.Item.lockedexp
                    }else{
                        result.status = 'allow';
                        result.count = data.Item.count
                        result.ip = clientip
                    }
                }else{
                    result.status = 'allow';
                    result.count = data.Item.count
                    result.ip = clientip
                }
                resolve(result);    
            }
        });
    });
}