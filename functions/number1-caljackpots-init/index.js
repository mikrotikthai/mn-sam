const AWS_REGION = process.env.AWS_REGION;
const TABLE_NUMBER1 = process.env.TABLE_NUMBER1;
const SNS_CAL_JACKPOTS = process.env.SNS_CAL_JACKPOTS

const AWS = require("aws-sdk");
const docClient = new AWS.DynamoDB.DocumentClient({region: AWS_REGION});
const sns = new AWS.SNS({region: AWS_REGION,apiVersion: '2010-03-31'});


var ResponseBadRequest = {
    statusCode: 400,
    body:JSON.stringify({"message":"Bad Request"})
};
var resultset,periods,recal;
exports.handle = function(event, context,callback) {
    console.log(JSON.stringify(event));
    let payload = JSON.parse(event.Records[0].Sns.Message);
    resultset = payload.resultset;
    periods = payload.periods;
    if('recal' in payload && payload.recal === true){
        recal = true;
    }else{
        recal = false;
    }
    calinit(recal,periods).then(function(){
        let wseg = [];
        for(var seg=0;seg<2;seg++){
            confirmkey = seg;
            wseg.push(publishSNS({resultset,periods,confirmkey},seg));
        }
        Promise.all(wseg).then(function(){
            callback(null,'ok');
        }).catch(function(err){
            console.error("error publish sns to seg cal"+JSON.stringify(err))
            callback(err)
        });
    }).catch(function(err){
        console.error("error delete old result for recal "+JSON.stringify(err))
        callback(err)
    });
};
function calinit(recal,periods){
    return new Promise((resolve, reject) => {
        if(recal===false){
            resolve();
        }else{
            console.log('recal naaaaaaaaaaa')
            resolve();

        } 
    });
}
function publishSNS(payload,segment){
    return new Promise((resolve, reject) => {
        var params = {
        Message: JSON.stringify(payload),
        TopicArn: SNS_CAL_JACKPOTS+"_seg"+segment
        };
        sns.publish(params, function(err, data) {
            if (err){
                console.log(err, err.stack); // an error occurred
                reject(err);
            }else{
                console.log(data);           // successful response
                resolve();
            }    
        });
    });
}