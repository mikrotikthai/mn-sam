# README #
ssh-keygen
ssh-keygen -f id_rsa.pub -e -m pem > pub1
aws s3 cp .\swagger.yaml s3://mikrotikthai/swagger.yaml
aws cloudformation package --template-file template.yaml --s3-bucket mikrotikthai --output-template-file packaged-template.yaml
aws cloudformation deploy --template-file D:\project\mn-sam\packaged-template.yaml --stack-name mytest --capabilities CAPABILITY_NAMED_IAM
this repo is obsolete
อันนี้ไม่ใช้แล้ว
ย้ายไป https://bitbucket.org/mikrotikthai/line-push-noti_webservice_lambda
move to https://bitbucket.org/mikrotikthai/line-push-noti_webservice_lambda


เป็น web service ที่ใช้เป็นตัวกลางสำหรับ รับข้อมูล notification จาก mikrotik และ ส่งการแจ้งเตื่อนผ่าน Line API 

**Version 0.1**


### install application dependencies  ###

```
#!python
virtualenv -p /usr/local/bin/python2.7 /home/appinven/public_html/linepushnoti/env
pip install -r requirements.txt
```

### require application dependencies(requirements.txt) ###

```
#!python
click==6.7
Flask==0.12
future==0.16.0
itsdangerous==0.24
Jinja2==2.9.3
line-bot-sdk==1.0.2
MarkupSafe==0.23
requests==2.12.4
Werkzeug==0.11.15

```